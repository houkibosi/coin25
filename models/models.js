const Sequelize = require('sequelize');
const sequelize = new Sequelize(
    // AWS
    'coin25db', 
    'coin25admin', 
    'korea201806', 
    {
       host: 'coin25dev.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',
 
        port: 3306,
        dialect: 'mysql',
        timezone: "+09:00",
        charset: 'utf8',
  
        collate: 'utf8_general_ci', 
        // timestamps: false, 
        supportBigNumbers: true
    }
    // 실서버
    // host: 'coin25newdb.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',  
    // 개발
    // host: 'coin25dev.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',

    // Local
    // 'coin25db', 
    // 'root', 
    // 'p@ssw0rd', 
    // {
    //     host: '127.0.0.1',
    //     port: 3306,
    //     dialect: 'mysql',
    //     timezone: "+09:00",
    //     supportBigNumbers: true,
    //     timestamps: true
    // }
);

const Op = Sequelize.Op;


// 1.사용자 정보 테이블
const User = sequelize.define('users_mastertbl', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        comment: "회원ID / Auto Increment"
    },
    email: {
        type: Sequelize.STRING(45),
        field: 'email',
        allowNull: false,
        comment: "회원의 email, ID 대신 사용."
    },
    type: {
        type: Sequelize.INTEGER(11),
        field: 'type',
        comment: "1:지점, 2:지점영업사원, 3: 일반회원, 0:관리자, 4: 광고주(ICO, 일반광고 포함)"
    },
    name: {
        type: Sequelize.STRING(45),
        field: 'name',
        allowNull: false,
        comment: ""
    },
    nickname: {
        type: Sequelize.STRING(40),
        field: 'nickname',
        comment: "닉네임 (사이트내에서 사용자들끼리 볼 수 있는 부분)"
    },
    recommender: {
        type: Sequelize.STRING(40),
        field: 'recommender',
        comment: "추천인의 전화번호로 추천인이 지점인 경우, partnerCode를 할당하고 다른 회원이 추천인을 조회할 때 partnerCode가 있으면 그것을 받아서 지점회원으로 자동가입"
    },
    jijum_contact: {
        type: Sequelize.STRING(25),
        field: 'jijum_contact',
        comment: "지점연락처 (일반회원이 추천인을 넣을때 지점인 경우 지점연락처 정보를 넣음)"
    },
    contact: {
        type: Sequelize.STRING(25),
        field: 'contact',
        comment: "회원휴대폰번호"
    },
    pwd: {
        type: Sequelize.STRING(25),
        field: 'pwd',
        comment: "회원비밀번호 (최대 20~25자리까지만)"
    },
    pay_point: {
        type: Sequelize.INTEGER(11
        ),
        field: 'pay_point',
        comment: "회원이 별도로 유료충전한 포인트합계"
    },
    free_point: {
        type: Sequelize.INTEGER(11),
        field: 'free_point',
        comment: "회원이 받은 무료증정 포인트합계"
    },
    b_delete: {
        type: Sequelize.INTEGER(1),
        field: 'b_delete',
        comment: "회원탈퇴여부 (회원정보 삭제안함)"
    },
    account_no: {
        type: Sequelize.STRING(45),
        field: 'account_no',
        comment: "회원이 지정한 은행계좌"
    },
    bank_code: {
        type: Sequelize.STRING(3),
        field: 'bank_code',
        comment: "국내은행코드"
    },
    createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
        comment: "회워가입시 생성일자 정보"
    },
    updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
        comment: "회원정보 변경시 변경일자 정보"
    },
    partner_code: {
        type: Sequelize.STRING(12),
        field: 'partner_code',
        comment: "추천인이 지점코드가 있는 회원인 경우, 해당 코드를 저장하여 연관관계를 형성, 지점코드 규칙: XX(국가))+XX(년도)+XXXX(일련번호) 국가: KR, JP, CH, US "
    },
    flag: {
        type: Sequelize.INTEGER(1),
        field: 'updated_at',
        comment: "보상처리전:0, 보상처리:1, 대량가입자: 9 <업체에서 추가>"
    }
});

// 2.포인트 정보 테이블
const Point = sequelize.define('point_mastertbls', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        comment: "Auto Increment"
    },
    users_idx: {
        type: Sequelize.BIGINT(20),
        field: 'users_idx',
        allowNull: false,
        comment: ""
    },
    email: {
        type: Sequelize.STRING(255),
        field: 'email',
    },
    state_div: {
        type: Sequelize.STRING(255),
        field: 'state_div',
        comment: "구분: 리워드, 충전, 사용"
    },
    usedPoint: {
        type: Sequelize.BIGINT(20),
        field: 'usedPoint',
        comment: "충전 및 리워드받은 금액"
    },
    point_detail: {
        type: Sequelize.STRING(255),
        field: 'point_detail',
        comment: "지급 및 포인트 사용 내역"
    },
    recommend_name: {
        type: Sequelize.STRING(255),
        field: 'recommend_name',
        comment: "지급 및 포인트 사용 내역"
    },
    recommend_email: {
        type: Sequelize.STRING(255),
        field: 'recommend_email',
        comment: ""
    },
    partner_code: {
        type: Sequelize.STRING(12),
        field: 'partner_code',
        comment: "지점 회원인 경우, 지점을 통해 지급받은 포인트인 경우 등록"
    },
    reg_date: {
        type: Sequelize.DATE,
        field: 'reg_date',
        comment: "지점 회원인 경우, 지점을 통해 지급받은 포인트인 경우 등록"
    }
});


// 3.커뮤니티/고객센터
const basic = sequelize.define('basic_boardtbl', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        comment: "Auto Increment"
    },
    categoryId: {
        type: Sequelize.STRING(10),
        field: 'categoryId',
        allowNull: false,
        comment: "게시판종류"
    },
    nickname: {
        type: Sequelize.STRING(40),
        field: 'nickname',
        allowNull: false,
        comment: "세션정보"
    },
    subject: {
        type: Sequelize.STRING(100),
        field: 'subject'
    },
    contents: {
        type: Sequelize.STRING(5000),
        field: 'contents'
    },
    cnt: {
        type: Sequelize.INTEGER(11),
        field: 'cnt',
        defaultValue: '0',
        comment: "기본값: 0, 상세화면들어갈때마다 카운트"
    },
    depth: {
        type: Sequelize.INTEGER(11),
        field: 'depth',
        comment: "구분: 0=사용안함, 1=사용함(댓글이 추가되면)"
    },
    filePath: {
        type: Sequelize.STRING(10),
        field: '파일경로 (차후 정리)'
    },
    regDate: {
        type: Sequelize.DATE,
        field: 'regDate',
        defaultValue: new Date()
    },
    user_idx: {
        type: Sequelize.BIGINT(20),
        field: 'user_idx',
        comment: "세션정보"
    }
});


// 7.판매구매
const buysell = sequelize.define('buysell_mastertbl', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',        
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    buysell_div: {
        type: Sequelize.STRING(1),
        field: 'buysell_div',
        allowNull: false
    },
    user_idx: {
        type: Sequelize.BIGINT(20),
        field: 'user_idx',
        allowNull: false
    },
    nickname: {
        type: Sequelize.STRING(40),
        field: 'nickname',
        allowNull: false
    },
    token_idx: {
        type: Sequelize.INTEGER(11),
        field: 'token_idx'
    },
    token_symbol: {
        type: Sequelize.STRING(20),
        field: 'token_symbol',
        defaultValue: '0'
    },
    token_price: {
        type: Sequelize.INTEGER(11),
        field: 'token_price'
    },
    token_qty: {
        type: Sequelize.DECIMAL(18),
        field: 'token_qty'
    },
    token_lowprice: {
        type: Sequelize.INTEGER(11),
        field: 'token_lowprice'
    },
    token_lowqty: {
        type: Sequelize.DECIMAL(18),
        field: 'token_lowqty'
    },
    reg_date: {
        type: Sequelize.DATE,
        field: 'reg_date'
    },
    end_date: {
        type: Sequelize.DATE,
        field: 'end_date'
    },
    token_proceed: {
        type: Sequelize.STRING(20),
        field: 'token_proceed'
    },
    buysell_top: {
        type: Sequelize.STRING(1),
        field: 'buysell_top'
    },
    buysell_subject: {
        type: Sequelize.STRING(45),
        field: 'buysell_subject'
    },
    buysell_content: {
        type: Sequelize.STRING(5000),
        field: 'buysell_content'
    }
});

// 8.판매구매신청
const buysell_trade = sequelize.define('buysell_tradetbl', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',        
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    buysell_idx: {
        type: Sequelize.BIGINT(20),
        field: 'buysell_idx',
        allowNull: false
    },
    buysell_div: {
        type: Sequelize.STRING(1),
        field: 'buysell_div'
    },
    trade_date: {
        type: Sequelize.DATE,
        field: 'trade_date'
    },
    token_symbol: {
        type: Sequelize.STRING(10),
        field: 'token_symbol'
    },
    trade_price: {
        type: Sequelize.INTEGER,
        field: 'trade_price'
    },
    token_qty: {
        type: Sequelize.DECIMAL,
        field: 'token_qty',
        defaultValue: '0'
    },
    trade_state: {
        type: Sequelize.STRING(10),
        field: 'trade_state'
    },
    trade_complete: {
        type: Sequelize.DATE,
        field: 'trade_complete'
    }
});

// 토큰정보
const tokenInfo = sequelize.define('token_infotbl', {
    id: {
        type: Sequelize.BIGINT,
        field: 'idx',        
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    token_krname: {
        type: Sequelize.STRING,
        field: 'token_krname',
        allowNull: false
    },
    token_enname: {
        type: Sequelize.STRING,
        field: 'token_enname',
        allowNull: false
    },
    token_symbol: {
        type: Sequelize.STRING,
        field: 'token_symbol',
        allowNull: false
    },
    token_listed: {

        type: Sequelize.STRING,
        field: 'token_listed'
    },
    token_logo_path: {
        type: Sequelize.STRING,
        field: 'token_logo_path',
        defaultValue: '0'
    },
    token_field: {
        type: Sequelize.STRING,
        field: 'token_field'
    },
    token_amount: {
        type: Sequelize.INTEGER,
        field: 'token_amount'
    },
    token_hardcap: {
        type: Sequelize.INTEGER,

        field: 'token_hardcap'
    },
    token_softcap: {
        type: Sequelize.INTEGER,
        field: 'token_softcap'
    },
    token_price: {
        type: Sequelize.STRING,
        field: 'token_price'
    },
    token_whitepaperUrl: {
        type: Sequelize.STRING,
        field: 'token_whitepaperUrl'
    },
    token_site: {
        type: Sequelize.STRING,
        field: 'token_site'
    },
    token_youtube: {
        type: Sequelize.STRING,
        field: 'token_youtube'
    },
    token_facebook: {
        type: Sequelize.STRING,
        field: 'token_facebook'
    },
    token_twitter: {
        type: Sequelize.STRING,
        field: 'token_twitter'
    },
    token_linkedin: {
        type: Sequelize.DATE,
        field: 'token_linkedin'
    },
    token_medium: {
        type: Sequelize.STRING,
        field: 'token_medium'
    },
    token_telegram_kr: {
        type: Sequelize.STRING,
        field: 'token_telegram_kr'
    },
    token_telegram_en: {
        type: Sequelize.STRING,
        field: 'token_telegram_en'
    },
    token_kakao: {
        type: Sequelize.STRING,
        field: 'token_kakao'
    },
    token_steemit: {
        type: Sequelize.STRING,
        field: 'token_steemit'
    },
    token_kyc: {
        type: Sequelize.STRING,
        field: 'token_kyc'
    },
    ico_started: {
        type: Sequelize.DATE,
        field: 'ico_started'
    },
    ico_end: {
        type: Sequelize.DATE,
        field: 'ico_end'
    },        
    ico_proceed: {
        type: Sequelize.STRING,
        field: 'ico_proceed'
    },
    token_tel: {
        type: Sequelize.STRING,
        field: 'token_tel'
    },
    token_address: {
        type: Sequelize.STRING,
        field: 'token_address'
    },
    token_email: {
        type: Sequelize.STRING,
        field: 'token_email'
    },
    token_ceo: {
        type: Sequelize.STRING,
        field: 'token_ceo'
    },
    ico_check: {
        type: Sequelize.STRING,
        field: 'ico_check'
    }
});



module.exports = {
    sequelize: sequelize,
    Op: Op,
    User: User,
    Point: Point,
    buysell: buysell,
    tokenInfo: tokenInfo,
    buysell_trade: buysell_trade
    // notice: notice,
    // basic: basic
};