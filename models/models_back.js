const Sequelize = require('sequelize');
const sequelize = new Sequelize(
    // AWS
    // 'coin25db', 
    // 'coin25admin', 
    // 'korea201806', 
    // {
    //    host: 'coin25dev.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',
 
    //     port: 3306,
    //     dialect: 'mysql',
    //     timezone: "+09:00",
    //     charset: 'utf8',
  
    //     collate: 'utf8_general_ci', 
    //     // timestamps: false, 
    //     supportBigNumbers: true
    // }
    // 실서버
    // host: 'coin25newdb.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',  
    // 개발
    // host: 'coin25dev.c9uwgsjsgybw.ap-northeast-2.rds.amazonaws.com',

    // Local
    'coin25db', 
    'root', 
    'p@ssw0rd', 
    {
        host: '127.0.0.1',
        port: 3306,
        dialect: 'mysql',
        timezone: "+09:00",
        supportBigNumbers: true,
        timestamps: true
    }
);

const Op = Sequelize.Op;


// 사용자 정보 테이블
const User = sequelize.define('users_mastertbl', {
    id: {
        type: Sequelize.BIGINT(20),
        field: 'idx',
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        comment: "회원ID / Auto Increment"
    },
    email: {
        type: Sequelize.STRING(45),
        field: 'email',
        allowNull: false,
        comment: "회원의 email, ID 대신 사용."
    },
    type: {
        type: Sequelize.INTEGER(11),
        field: 'type',
        comment: "1:지점, 2:지점영업사원, 3: 일반회원, 0:관리자, 4: 광고주(ICO, 일반광고 포함)"
    },
    name: {
        type: Sequelize.STRING(45),
        field: 'name',
        allowNull: false,
        comment: ""
    },
    nickname: {
        type: Sequelize.STRING(40),
        field: 'nickname',
        comment: "닉네임 (사이트내에서 사용자들끼리 볼 수 있는 부분)"
    },
    recommender: {
        type: Sequelize.STRING(40),
        field: 'recommender',
        comment: "추천인의 전화번호로 추천인이 지점인 경우, partnerCode를 할당하고 다른 회원이 추천인을 조회할 때 partnerCode가 있으면 그것을 받아서 지점회원으로 자동가입"
    },
    jijum_contact: {
        type: Sequelize.STRING(25),
        field: 'jijum_contact',
        comment: "지점연락처 (일반회원이 추천인을 넣을때 지점인 경우 지점연락처 정보를 넣음)"
    },
    contact: {
        type: Sequelize.STRING(25),
        field: 'contact',
        comment: "회원휴대폰번호"
    },
    pwd: {
        type: Sequelize.STRING(25),
        field: 'pwd',
        comment: "회원비밀번호 (최대 20~25자리까지만)"
    },
    pay_point: {
        type: Sequelize.INTEGER(11
        ),
        field: 'pay_point',
        comment: "회원이 별도로 유료충전한 포인트합계"
    },
    free_point: {
        type: Sequelize.INTEGER(11),
        field: 'free_point',
        comment: "회원이 받은 무료증정 포인트합계"
    },
    b_delete: {
        type: Sequelize.INTEGER(1),
        field: 'b_delete',
        comment: "회원탈퇴여부 (회원정보 삭제안함)"
    },
    account_no: {
        type: Sequelize.STRING(45),
        field: 'account_no',
        comment: "회원이 지정한 은행계좌"
    },
    bank_code: {
        type: Sequelize.STRING(3),
        field: 'bank_code',
        comment: "국내은행코드"
    },
    createdAt: {
        type: Sequelize.DATE,
        field: 'created_at',
        comment: "회워가입시 생성일자 정보"
    },
    updatedAt: {
        type: Sequelize.DATE,
        field: 'updated_at',
        comment: "회원정보 변경시 변경일자 정보"
    },
    partner_code: {
        type: Sequelize.STRING(12),
        field: 'partner_code',
        comment: "추천인이 지점코드가 있는 회원인 경우, 해당 코드를 저장하여 연관관계를 형성, 지점코드 규칙: XX(국가))+XX(년도)+XXXX(일련번호) 국가: KR, JP, CH, US "
    },
    flag: {
        type: Sequelize.INTEGER(1),
        field: 'updated_at',
        comment: "보상처리전:0, 보상처리:1, 대량가입자: 9 <업체에서 추가>"
    },


    // id: {
    //     type: Sequelize.BIGINT,
    //     field: 'idx',
    //     allowNull: false,
    //     autoIncrement: true,
    //     primaryKey: true
    // },
    // email: {
    //     type: Sequelize.STRING,
    //     field: 'email',
    //     allowNull: false
    // },    
    // type: {
    //     type: Sequelize.INTEGER,
    //     field: 'type'
    // },    
    // name: {
    //     type: Sequelize.STRING,
    //     field: 'name',
    //     allowNull: false
    // },    
    // nickname: {
    //     type: Sequelize.STRING,
    //     field: 'nickname'
    // },    
    // recommender: {
    //     type: Sequelize.STRING,
    //     field: 'recommender'
    // },    
    // jijum_contact: {
    //     type: Sequelize.STRING,
    //     field: 'jijum_contact'
    // },    
    // contact: {
    //     type: Sequelize.STRING,
    //     field: 'contact'
    // },    
    // pwd: {
    //     type: Sequelize.STRING,
    //     field: 'pwd',
    //     allowNull: false
    // },    
    // pay_point: {
    //     type: Sequelize.INTEGER,
    //     field: 'pay_point',
    //     allowNull: false
    // },    
    // free_point: {
    //     type: Sequelize.INTEGER,
    //     field: 'free_point',
    //     allowNull: false
    // },    
    // b_delete: {
    //     type: Sequelize.BOOLEAN,
    //     field: 'b_delete'
    // },    
    // bank_code: {
    //     type: Sequelize.STRING,
    //     field: 'bank_code'
    // },    
    // account_no: {
    //     type: Sequelize.STRING,
    //     field: 'account_no'
    // },    
    // createdAt: {
    //     type: Sequelize.DATE,
    //     field: 'created_at'
    // },    
    // updatedAt: {
    //     type: Sequelize.DATE,
    //     field: 'updated_at'
    // },    
    // partner_code: {
    //     type: Sequelize.STRING,
    //     field: 'partner_code'
    // },    
    // flag: {
    //     type: Sequelize.INTEGER,
    //     field: 'flag'
    // }
});

// 포인트 정보 테이블
const Point = sequelize.define('point_mastertbls', {
    id: {
        type: Sequelize.BIGINT,
        field: 'idx',
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    user_idx: {
        type: Sequelize.BIGINT,
        field: 'user_idx',
        allowNull: false
    },    
    email: {
        type: Sequelize.STRING,
        field: 'email',
        allowNull: false
    },    
    state_div: {
        type: Sequelize.STRING,
        field: 'state_div'
    },    
    used_point: {
        type: Sequelize.BIGINT,
        field: 'used_point'
    },    
    point_detail: {
        type: Sequelize.STRING,
        field: 'point_detail'
    },    
    recommend_name: {
        type: Sequelize.STRING,
        field: 'recommend_name'
    },    
    recommend_email: {
        type: Sequelize.STRING,
        field: 'recommend_email'
    },    
    partner_code: {
        type: Sequelize.STRING,
        field: 'partner_code'
    },    
    createdAt: {
        type: Sequelize.DATE,
        field: 'reg_date'
    },
    updatedAt: {
        type: Sequelize.DATE,
        field: 'reg_date'
    }
});


// 커뮤니티/고객센터
// const basic = sequelize.define('basic_boardtbl', {
//     categoryId: {
//         type: Sequelize.STRING,
//         field: 'categoryId',
//         allowNull: false
//     },
//     nickname: {
//         type: Sequelize.STRING,
//         field: 'nickname',
//         allowNull: false
//     },
//     subject: {
//         type: Sequelize.STRING,
//         field: 'subject',
//         allowNull: false
//     },
//     contents: {
//         type: Sequelize.STRING,
//         field: 'contents'
//     },
//     cnt: {
//         type: Sequelize.INTEGER,
//         field: 'cnt',
//         defaultValue: '0'
//     },
//     depth: {
//         type: Sequelize.INTEGER,
//         field: 'depth'
//     },
//     depthOff: {
//         type: Sequelize.CHAR,
//         field: 'depthOff'
//     },
//     filePath: {
//         type: Sequelize.CHAR,
//         field: 'filePath'
//     },
//     regDate: {
//         type: Sequelize.DATE,
//         field: 'regDate',
//         defaultValue: new Date()
//     },user_idx: {
//         type: Sequelize.BOOLEAN,
//         field: 'user_idx'
//     }
// });


// 판매구매
const buysell = sequelize.define('buysell_mastertbl', {
    id: {
        type: Sequelize.INTEGER,
        field: 'idx',        
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    buysell_div: {
        type: Sequelize.STRING,
        field: 'buysell_div',
        allowNull: false
    },
    user_idx: {
        type: Sequelize.BIGINT,
        field: 'user_idx',
        allowNull: false
    },
    nickname: {
        type: Sequelize.STRING,
        field: 'nickname',
        allowNull: false
    },
    token_idx: {
        type: Sequelize.INTEGER,
        field: 'token_idx'
    },
    token_symbol: {
        type: Sequelize.STRING,
        field: 'token_symbol',
        defaultValue: '0'
    },
    token_price: {
        type: Sequelize.INTEGER,
        field: 'token_price'
    },
    token_qty: {
        type: Sequelize.DECIMAL,
        field: 'token_qty'
    },
    token_lowprice: {
        type: Sequelize.INTEGER,
        field: 'token_lowprice'
    },
    token_lowqty: {
        type: Sequelize.DECIMAL,
        field: 'token_lowqty'
    },
    reg_date: {
        type: Sequelize.DATE,
        field: 'reg_date'
    },
    end_date: {
        type: Sequelize.DATE,
        field: 'end_date'
    },
    token_proceed: {
        type: Sequelize.STRING,
        field: 'token_proceed'
    },
    buysell_top: {
        type: Sequelize.STRING,
        field: 'buysell_top'
    },
    buysell_subject: {
        type: Sequelize.STRING,
        field: 'buysell_subject'
    },
    buysell_content: {
        type: Sequelize.STRING,
        field: 'buysell_content'
    }
});

// 토큰정보
const tokenInfo = sequelize.define('token_infotbl', {
    id: {
        type: Sequelize.BIGINT,
        field: 'idx',        
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    token_krname: {
        type: Sequelize.STRING,
        field: 'token_krname',
        allowNull: false
    },
    token_enname: {
        type: Sequelize.STRING,
        field: 'token_enname',
        allowNull: false
    },
    token_symbol: {
        type: Sequelize.STRING,
        field: 'token_symbol',
        allowNull: false
    },
    token_listed: {

        type: Sequelize.STRING,
        field: 'token_listed'
    },
    token_logo_path: {
        type: Sequelize.STRING,
        field: 'token_logo_path',
        defaultValue: '0'
    },
    token_field: {
        type: Sequelize.STRING,
        field: 'token_field'
    },
    token_amount: {
        type: Sequelize.INTEGER,
        field: 'token_amount'
    },
    token_hardcap: {
        type: Sequelize.INTEGER,

        field: 'token_hardcap'
    },
    token_softcap: {
        type: Sequelize.INTEGER,
        field: 'token_softcap'
    },
    token_price: {
        type: Sequelize.STRING,
        field: 'token_price'
    },
    token_whitepaperUrl: {
        type: Sequelize.STRING,
        field: 'token_whitepaperUrl'
    },
    token_site: {
        type: Sequelize.STRING,
        field: 'token_site'
    },
    token_youtube: {
        type: Sequelize.STRING,
        field: 'token_youtube'
    },
    token_facebook: {
        type: Sequelize.STRING,
        field: 'token_facebook'
    },
    token_twitter: {
        type: Sequelize.STRING,
        field: 'token_twitter'
    },
    token_linkedin: {
        type: Sequelize.DATE,
        field: 'token_linkedin'
    },
    token_medium: {
        type: Sequelize.STRING,
        field: 'token_medium'
    },
    token_telegram_kr: {
        type: Sequelize.STRING,
        field: 'token_telegram_kr'
    },
    token_telegram_en: {
        type: Sequelize.STRING,
        field: 'token_telegram_en'
    },
    token_kakao: {
        type: Sequelize.STRING,
        field: 'token_kakao'
    },
    token_steemit: {
        type: Sequelize.STRING,
        field: 'token_steemit'
    },
    token_kyc: {
        type: Sequelize.STRING,
        field: 'token_kyc'
    },
    ico_started: {
        type: Sequelize.DATE,
        field: 'ico_started'
    },
    ico_end: {
        type: Sequelize.DATE,
        field: 'ico_end'
    },        
    ico_proceed: {
        type: Sequelize.STRING,
        field: 'ico_proceed'
    },
    token_tel: {
        type: Sequelize.STRING,
        field: 'token_tel'
    },
    token_address: {
        type: Sequelize.STRING,
        field: 'token_address'
    },
    token_email: {
        type: Sequelize.STRING,
        field: 'token_email'
    },
    token_ceo: {
        type: Sequelize.STRING,
        field: 'token_ceo'
    },
    ico_check: {
        type: Sequelize.STRING,
        field: 'ico_check'
    }
});



module.exports = {
    sequelize: sequelize,
    Op: Op,
    User: User,
    Point: Point,
    buysell: buysell,
    tokenInfo: tokenInfo
    // notice: notice,
    // basic: basic
};