// var moment = impoet('/front3/assets/app/js/moment.js');
// var moment = require('moment');
var moment = document.write("<script src='/assets/app/js/moment.js'></script>");

var lang_kor = { "decimal" : "", "emptyTable" : "데이터가 없습니다.", "info" : "", "infoEmpty" : "", "infoFiltered" : "", "infoPostFix" : "", "thousands" : ",", "lengthMenu" : "", "loadingRecords" : "로딩중...", "processing" : "처리중...", "search" : "검색 : ", "zeroRecords" : "검색된 데이터가 없습니다.", "paginate" : { "first" : "처음", "last" : "마지막", "next" : "다음", "previous" : "이전" }, "aria" : { "sortAscending" : " :  오름차순 정렬", "sortDescending" : " :  내림차순 정렬" }};
var DatatablesSearchOptionsColumnSearch = function () {
    $.fn.dataTable.Api.register("column().title()", function (row) {
        return $(this.header()).text().trim()
    });
    return {
        init: function () {
            var t;
            t = $("#m_table_1").DataTable({

                responsive: !0,
                // dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                lengthMenu: [5, 10, 25, 50],
                pageLength: 10,
                bPaginate : true,  	// 페이징
                language: lang_kor,
                searchDelay: 500,
                processing: !0,
                searching: false,
                ordering: true,
                // serverSide: !0,
                ajax: 
                {
                    url: "/points",
                    type: "GET"
                },
                columns: [{
                    data: "sState"
                }, {
                    data: "nUsedPoint"
                }, {
                    data: "nRemainPoint"
                }, {
                    data: "sDetail"
                }, {
                    data: "createdAt"
                }]
            })
        }
    }
}();
jQuery(document).ready(function () {
    DatatablesSearchOptionsColumnSearch.init()
});