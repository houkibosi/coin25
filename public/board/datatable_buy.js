var lang_kor = { "decimal" : "", "emptyTable" : "데이터가 없습니다.", "info" : "", "infoEmpty" : "", "infoFiltered" : "", "infoPostFix" : "", "thousands" : ",", "lengthMenu" : "", "loadingRecords" : "로딩중...", "processing" : "처리중...", "search" : "검색 : ", "zeroRecords" : "검색된 데이터가 없습니다.", "paginate" : { "first" : "처음", "last" : "마지막", "next" : "다음", "previous" : "이전" }, "aria" : { "sortAscending" : " :  오름차순 정렬", "sortDescending" : " :  내림차순 정렬" }};
var e, index=1;
var DatatablesBasicBasic=
{
    init:function()
    {
        (
            e=$("#m_table_1")).DataTable(
                {
                    responsive:!0,
                    dom:"<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu:[5,10,25,50],
                    pageLength:10,
                    language:lang_kor,
                    order:[[0,"desc"]],
                    "ajax": {
                        "url": "/mybuy/" + $("#hdIdx").val(),
                        "dataSrc": "data"
                    },
                    columns: [
                        { 
                            data: "idx",
                            render: function(data, type, row) {
                                return data;
                            }
                        },
                        { 
                            data: "order_no",
                            render: function(data, type, row) {
                                return '0';
                            }
                        },
                        { 
                            data: "token_krname",
                            render: function(data, type, row) {
                                return data + " (" + row.token_symbol + ")";
                            }
                        },
                        { 
                            data: "token_qty",
                            render: function(data, type, row) {
                                return data + " (" + row.token_symbol + ")";
                            }
                        },
                        { 
                            data: "trade_price",
                            render: function(data, type, row) {
                                return data + " 원";
                            }
                        },
                        { 
                            data: "nickname",
                            render: function(data, type, row) {
                                return data;
                            }
                        },
                        { 
                            data: "token_wallet_address",
                            render: function(data, type, row) {
                                return data;
                            }
                        },
                        { 
                            data: "trade_date",
                            render: function(data, type, row) {
                                return (moment(data).format("YYYY-MM-DD HH:mm"));
                            }
                        },
                        { 
                            data: "trade_state",
                            render: function(data, type, row) {
                                var state = data.trade_state;
                                if(state == "구매신청")
                                {
                                    return '구매 신청';
                                }
                                else if(state == "거래승인")	
                                {
                                    return '입금 요청';
                                }
                                else if(state == "거래거절")	
                                {
                                    return '거래 거절';
                                }
                                else if(state == "입금요청")	
                                {                                   
                                    return '<button type="button" class="btn btn-outline-success m-btn m-btn--outline-2x" onclick="depositOk('+row.idx+')">입 금</button>' + 
                                    '<button type="button" class="btn btn-outline-danger m-btn m-btn--outline-2x" onclick=$("#checkAccountModal").modal("show");>계좌 보기</button>';  
                                }
                                else if(state == "입금확인")	
                                {
                                    return '입금 확인';
                                }
                                else if(state == "토큰전송요청")	
                                {
                                    return '토큰전송 요청';
                                }
                                else if(state == "토큰전송완료")	
                                {
                                    return '<button type="button" class="btn btn-outline-info m-btn m-btn--outline-2x" onclick="dealcomplete('+row.idx+')">수 령</button>';
                                }
                                else if(state == "코인수령")	
                                {
                                    return '대금 정산 중';
                                }
                                else if(state == "판매대금입금")	
                                {
                                    return '대금 입금 중';
                                }
                                else if(state == "거래완료")	
                                {
                                    return '거래 완료';
                                }
                            }
                        }
                    ],
                }
            )
        }
    };
    jQuery(document).ready(function(){
        DatatablesBasicBasic.init();
        new $.fn.dataTable.FixedHeader( e );
    }
);