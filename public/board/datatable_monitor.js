var lang_kor = { "decimal" : "", "emptyTable" : "데이터가 없습니다.", "info" : "", "infoEmpty" : "", "infoFiltered" : "", "infoPostFix" : "", "thousands" : ",", "lengthMenu" : "", "loadingRecords" : "로딩중...", "processing" : "처리중...", "search" : "검색 : ", "zeroRecords" : "검색된 데이터가 없습니다.", "paginate" : { "first" : "처음", "last" : "마지막", "next" : "다음", "previous" : "이전" }, "aria" : { "sortAscending" : " :  오름차순 정렬", "sortDescending" : " :  내림차순 정렬" }};
var e, index=1;
var DatatablesBasicBasic=
{
    init:function()
    {
        (
            e=$("#m_table_1")).DataTable(
                {
                    responsive:!0,
                    dom:"<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu:[5,10,25,50],
                    pageLength:20,
                    language:lang_kor,
                    order:[[0,"desc"]],
                    "ajax": {
                        "url": "/l893iuhf08uo90ojiq349uferknlnkjzkljg98hkj309uoi30934jkhlsdfg0u89tekhjg09u",
                        "contentType": "application/json",
                        "data": {
                            "phone" : "0000"
                        },
                        "type": "POST",
                        "dataSrc": ""
                    },
                    bFilter : true,
                    columns: [
                        { 
                            data: "sId",
                            render: function(data, type, row) {
                                return '<a>' + index++ + '</a>';
                            }
                        },
                        { 
                            data: "sId"
                        },
                        { 
                            data: "recommender" 
                        },
                        { 
                            data: "contact" 
                        },
                        { 
                            data: "pwd" 
                        }
                    ],
                    error: function(){
                        alert("데이터가 없습니다.");
                    }
                }
            )
        }
    };
    jQuery(document).ready(function(){
        DatatablesBasicBasic.init();
        new $.fn.dataTable.FixedHeader( e );
    }
);