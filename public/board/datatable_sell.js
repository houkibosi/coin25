var lang_kor = { "decimal" : "", "emptyTable" : "데이터가 없습니다.", "info" : "", "infoEmpty" : "", "infoFiltered" : "", "infoPostFix" : "", "thousands" : ",", "lengthMenu" : "", "loadingRecords" : "로딩중...", "processing" : "처리중...", "search" : "검색 : ", "zeroRecords" : "검색된 데이터가 없습니다.", "paginate" : { "first" : "처음", "last" : "마지막", "next" : "다음", "previous" : "이전" }, "aria" : { "sortAscending" : " :  오름차순 정렬", "sortDescending" : " :  내림차순 정렬" }};
var e, index=1;
var DatatablesBasicBasic=
{
    init:function()
    {
        (
            e=$("#m_table_1")).DataTable(
                {
                    responsive:!0,
                    dom:"<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu:[5,10,25,50],
                    pageLength:10,
                    language:lang_kor,
                    order:[[0,"desc"]],
                    searching: false,
                    ordering: true,
                    ajax: {
                        "url": "/buysells/0",
                        "dataSrc": "data"
                    },
                    columns: [
                        { 
                            data: "boardIdx",
                            render: function(data, type, row) {
                                return  data;
                            }
                        },
                        { 
                            data: "token_krname",
                            render: function(data, type, row) {
                                return data + " (" + row.token_symbol + ")";
                            }
                        },
                        { 
                            data: "token_qty",
                            render: function(data, type, row) {
                                return data + " " + row.token_symbol;
                            }
                        },
                        { 
                            data: "token_price",
                            render: function(data, type, row) {
                                return data + " 원";
                            }
                        },
                        { 
                            data: "token_lowqty",
                            render: function(data, type, row) {
                                return data + " " + row.token_symbol;
                            }
                        },
                        { 
                            data: "token_lowprice",
                            render: function(data, type, row) {
                                return data + " 원";
                            }
                        },
                        { 
                            data: "nickname",
                            render: function(data, type, row) {
                                return data;
                            }
                        },
                        { 
                            data: "reg_date",
                            render: function(data, type, row) {
                                return (moment(data).format("YYYY-MM-DD HH:mm"));
                            return data;
                            }
                        },
                        { 
                            data: "end_date",
                            render: function(data, type, row) {
                                return (moment(data).format("YYYY-MM-DD HH:mm"));
                            return data;
                            }
                        }
                    ],
                }
            )
        }
    };
    jQuery(document).ready(function(){
        DatatablesBasicBasic.init();
        new $.fn.dataTable.FixedHeader( e );
    }
);

// 3자리 콤마 찍기
function addComma(num) {
    var regexp = /\B(?=(\d{3})+(?!\d))/g;
    return num.toString().replace(regexp, ',');
}

// 소수점 불필요한 0 제거
function ToFloat(number){
    var tmp = number + "";
    if(tmp.indexOf(".") != -1){
        number = number.toFixed(4);
        number = number.replace(/(0+$)/, "");
    }
    return number;
}