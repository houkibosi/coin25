var lang_kor = { "decimal" : "", "emptyTable" : "데이터가 없습니다.", "info" : "", "infoEmpty" : "", "infoFiltered" : "", "infoPostFix" : "", "thousands" : ",", "lengthMenu" : "", "loadingRecords" : "로딩중...", "processing" : "처리중...", "search" : "검색 : ", "zeroRecords" : "검색된 데이터가 없습니다.", "paginate" : { "first" : "처음", "last" : "마지막", "next" : "다음", "previous" : "이전" }, "aria" : { "sortAscending" : " :  오름차순 정렬", "sortDescending" : " :  내림차순 정렬" }};
var DatatablesBasicBasic=
{
    init:function()
    {
        var e;
        (
            e=$("#m_table_1")).DataTable(
                {
                    responsive:!0,
                    dom:"<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                    lengthMenu:[5,10,25,50],
                    pageLength:10,
                    language:lang_kor,
                    order:[[1,"desc"]],
                    "ajax": {
                        "url": "json/auction.json",
                        "dataSrc": "demo"
                    }
                }
            )
        }
    };
    jQuery(document).ready(function(){DatatablesBasicBasic.init()
    }
);