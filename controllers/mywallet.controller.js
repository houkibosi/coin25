const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
  const myIdx = req.params.id;

  if (!myIdx) {
    return res.status(400).json({error: 'Incorrect id'});
  }

    var qStr = "";
    // qStr += "select * from buysell_tradetbl";
    // qStr += "select bm.idx, bt.idx, bm.token_symbol, ti.token_krname, ti.token_enname, bm.token_price, bm.token_qty, bm.token_lowqty, bm.token_lowprice, bm.nickname, bt.trade_state ";
    // qStr += "from buysell_tradetbl bt inner join buysell_mastertbl bm on bt.buysell_idx = bm.idx left outer join token_infotbl ti on bm.token_idx = ti.idx ";
    // qStr += "where bt.user_idx = " + myIdx;
    qStr += "select * ";
    qStr += "from mywallet_infotbl where user_idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const wallet_id = req.params.wid;
  const user_idx = req.params.id;
  

  var qStr = "";
  qStr += "select * from mywallet_infotbl where "+user_idx+" = 8502 and idx = " + wallet_id
  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(data => res.json(
    {
      'data' : data
    }
  ));
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.buysell.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const token_idx = parseInt(req.body.token_idx, 10) || '';
    const token_wallet_address = req.body.token_wallet_address || '';
    const user_idx = parseInt(req.body.user_idx, 10) || '';
    const token_symbol = req.body.token_symbol || '';
    
    var qStr = "";
    qStr += "insert into mywallet_infotbl (token_idx, token_wallet_address, user_idx, token_symbol) ";
    qStr += "values("+token_idx+", '"+token_wallet_address+"', "+user_idx+", '"+token_symbol+"') ";

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  const idx = req.body.idx;
  const trade_state = req.body.trade_state;

  var qStr = "";
  qStr += "update buysell_tradetbl set trade_state = '"+trade_state+"' where idx = "+idx;

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})  
  .then((user) => res.status(200).json(user))    
  }

  