const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
    models.basic.findAll({
      // where : {
      //   bDelete: {
      //     [Op.ne]: 1
      //   }
      // }
    })
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const id = parseInt(req.params.id, 10);
  if (!id) {
    return res.status(400).json({error: 'Incorrect id'});
  }

  models.notice.findOne({
    where: {
      id: id
    }
  }).then(notice => {
    if (!notice) {
      return res.status(404).json({error: 'No id'});
    }

    return res.json(notice);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const userid = parseInt(req.params.userid, 10);
    if (!userid) {
      return res.status(400).json({error: 'Incorrect userid'});
    }
  
    models.notice.update({
      bDelete: '1'
      }, {
      where: {
        userid: userid
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
  const categoryId = req.body.categoryId || '';
  const nickname = req.body.nickname || '';
  const subject = req.body.subject || '';
  const contents = req.body.contents || '';
  const cnt = req.body.cnt || '';
  const depth = req.body.depth || '';
  const depthOff = req.body.depthOff || '';
  const filePath = req.body.filePath || '';
  const regDate = req.body.regDate || '';
  const user_idx = req.body.user_idx || '';

  if (!user_idx) {
    return res.status(400).json({error: 'Incorrect user_idx'});
  }

  var queryStr = "";
  

  models.sequelize.query(queryStr, { type: sequelize.QueryTypes.SELECT})
  .then((notice) => res.status(201).json(notice))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const sIp = body.sIp;
  const name = body.name;
  const phone = body.phone;
  const password = body.password;
  const admin = body.admin;
  const address = body.address;
  const inday = body.inday;
  const outday = body.outday;
  const email = body.email;
  const team = body.team;
  const worktime = parseInt(body.worktime, 10);
  const unuselevel = parseInt(body.unuselevel, 10);

  models.notice.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((notice) => {
    models.notice.update(
      {
        phone: phone? phone : notice.phone,
        password: password? password : notice.password,
        admin: admin? admin : notice.admin,
        worktime: worktime? worktime : notice.worktime,
        inday: inday? inday : notice.inday,
        outday: outday? outday : notice.outday,
        worktime: worktime? worktime : notice.worktime,
        unuselevel: unuselevel? unuselevel : notice.unuselevel,
        email: email? email : notice.email,
        team: team? team : notice.team
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((notice) => res.status(200).json(notice))
  })    
  }

  