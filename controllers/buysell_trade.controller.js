const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
    const buysell_div = req.params.buysell_div;
    // models.buysell.findAll({
    //   where : {
    //     buysell_div: buysell_div
    //     // bDelete: {
    //     //   [Op.ne]: 1
    //     // }
    //   }
    // })
    var qStr = "";
    qStr += "SELECT idx, buysell_subject, nickname, reg_date FROM buysell_mastertbl where buysell_div = "+buysell_div+";";

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const buysell_div = req.params.buysell_div;
  const id = req.params.id;
  

  // models.buysell.findOne({
  //   where: {
  //     sId: sId
  //   }
  // })
  var qStr = "";
  qStr += "select * from buysell_mastertbl where buysell_div = "+buysell_div+" and idx = "+id+";";

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.buysell.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const buysell_div = req.body.buysell_div || '';
    const buysell_idx = req.body.buysell_idx || '';
    const token_symbol = req.body.token_symbol || '';
    const trade_price = parseInt(req.body.trade_price, 10) || '0';
    const token_qty = req.body.token_qty || '';
    const trade_state = req.body.trade_state || '';
    const trade_complete = req.body.trade_complete || '';
    const user_idx = req.body.user_idx || '';
    const nickname = req.body.nickname || '';
    const buyer_tel = req.body.buyer_tel || '';
    const seller_tel = req.body.seller_tel || '';

    // str += "select a.email as email, b.state_div as state_div, b.used_point as used_point, b.point_detail as point_detail, DATE_FORMAT(b.reg_date, '%Y.%c.%d %H:%i') as createdAt, substring(b.recommend_name,1,2) as recommend_name ";
    // str += "from users_mastertbls a left outer join point_mastertbls b on a.email = b.email where contact like '"+phone+"' order by createdAt desc";
    var qStr = "";
    qStr += "insert into buysell_tradetbl (buysell_idx, buysell_div, trade_date, token_symbol, trade_price, token_qty, trade_state, trade_complete, user_idx, nickname, buyer_tel, seller_tel) ";
    qStr += "values('"+buysell_idx+"', '"+buysell_div+"', now(), '"+token_symbol+"', "+trade_price+", "+token_qty+", '"+trade_state+"', '"+trade_complete+"', '"+user_idx+"', '"+nickname+"', '"+buyer_tel+"', '"+seller_tel+"') ";

    //console.log("!!!!!"+str);
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then((user) => res.status(201).json(user));
}
// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const name = req.body.name || '';
  const recommender = req.body.recommender || '';
  const contact = req.body.contact || '';
  const pwd = req.body.pwd || '';
  const sId = req.body.sId || '';
  sId_back = sId;
  const email = req.body.email || '';
  const c_key = req.body.c_key || '';
  const pay_point = parseInt(req.body.pay_point, 10) || '0';
  const free_point = parseInt(req.body.free_point, 10) || '0';
  const bank_adress = req.body.worktime || '';
  const jijum_contact = req.body.jijum_contact || '';
  const type = req.body.type || '3';

  models.buysell.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((user) => {
    models.User.update(
      {
        name: name ? name : User.name,
        recommender: recommender ? recommender : User.recommender,
        contact: contact ? contact : User.contact,
        pwd: pwd ? pwd : User.contact,
        sId: sId ? sId : User.sId,
        email: email ? email : User.email,
        c_key: c_key ? c_key : User.c_key,
        pay_point: pay_point ? pay_point : User.pay_point,
        free_point: free_point ? free_point : User.pay_point,
        bank_adress: bank_adress ? bank_add : User.bank_adress,
        jijum_contact: jijum_contact ? jijum_contact : User.jijum_contact,
        type: type
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((user) => res.status(200).json(user))
  })    
  }

  