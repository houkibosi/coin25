const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
    const buysell_div = req.params.buysell_div;
    // models.buysell.findAll({
    //   where : {
    //     buysell_div: buysell_div
    //     // bDelete: {
    //     //   [Op.ne]: 1
    //     // }
    //   }
    // })
    var qStr = "";
    // qStr += "SELECT idx, buysell_subject, nickname, reg_date FROM buysell_mastertbl where buysell_div = "+buysell_div+";";
    // qStr += "SELECT bm.idx, ti.token_krname, ti.token_symbol, bm.token_price, bm.token_qty, bm.token_lowqty, bm.token_lowprice, bm.nickname, end_date ";
    // qStr += "from buysell_mastertbl bm join token_infotbl ti on bm.token_idx = ti.idx where bm.buysell_div = " + buysell_div;
    qStr += "select bm.idx as boardIdx, ti.token_krname, bm.token_symbol, bm.token_qty, bm.token_price, bm.token_lowqty, bm.token_lowprice, bm.nickname, bm.reg_date, bm.end_date ";
    qStr += "from buysell_mastertbl bm inner join token_infotbl ti on ti.idx = bm.token_idx ";
    // qStr += "where bm.user_idx = " + id;

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const buysell_div = req.params.buysell_div;
  const id = req.params.id;
  

  // models.buysell.findOne({
  //   where: {
  //     sId: sId
  //   }
  // })
  var qStr = "";
  qStr += "select * from buysell_mastertbl where buysell_div = "+buysell_div+" and idx = "+id+";";
  // qStr += "select seller.name as sellerName, seller.nickname, bm.token_symbol, bm.token_price, bm.token_qty, mi.token_wallet_address as wAddress, bm.reg_date, bm.token_proceed as token_proceed, bm.end_date ";
  // qStr += "from buysell_mastertbl bm inner join users_mastertbls seller on bm.user_idx = seller.idx inner join mywallet_infotbl mi on mi.user_idx = seller.idx ";
  // qStr += "where bm.user_idx = " + id;
  
  

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.buysell.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const buysell_div = req.body.buysell_div || '';
    const token_idx = parseInt(req.body.token_idx, 10) || '0';
    const token_symbol = req.body.token_symbol || '';
    const token_price = parseInt(req.body.token_price, 10) || '0';
    const token_qty = req.body.token_qty || '';
    const token_lowprice = parseInt(req.body.token_lowprice, 10) || '0';
    const token_lowqty = req.body.token_lowqty || '';
    const reg_date = req.body.reg_date || false;
    const end_date = parseInt(req.body.end_date, 10) || '';
    const token_proceed = req.body.token_proceed || '';
    const buysell_top = req.body.buysell_top || '';
    const buysell_subject = req.body.buysell_subject || '';
    const buysell_content = req.body.buysell_content || '';

    const user_idx = parseInt(req.body.user_idx) || '';
    const nickname = req.body.nickname || '';
    const seller_tel = req.body.seller_tel || '';

    const order_no = req.body.order_no || '';

    console.log(seller_tel);

    var doDate = 0;
    if(end_date == -500){
      doDate = 1;
    } else if(end_date == -1500) {
      doDate = 3; 
    } else if(end_date == -3000) {
      doDate = 7;
    }
    
    // models.buysell.create({      
    //   buysell_div: buysell_div,
    //   token_idx: token_idx,
    //   token_symbol: token_symbol,
    //   token_price: token_price,
    //   token_qty: token_qty,
    //   token_lowprice: token_lowprice,
    //   token_lowqty: token_lowqty,
    //   reg_date: reg_date,
    //   end_date: end_date,
    //   token_proceed: token_proceed,
    //   buysell_top: buysell_top,
    //   buysell_subject: buysell_subject,
    //   buysell_content: buysell_content,

    //   user_id: user_idx,
    //   nickname: nickname
    // })
    // str += "select a.email as email, b.state_div as state_div, b.used_point as used_point, b.point_detail as point_detail, DATE_FORMAT(b.reg_date, '%Y.%c.%d %H:%i') as createdAt, substring(b.recommend_name,1,2) as recommend_name ";
    // str += "from users_mastertbls a left outer join point_mastertbls b on a.email = b.email where contact like '"+phone+"' order by createdAt desc";
    var qStr = "";
    qStr += "insert into buysell_mastertbl (buysell_div, token_idx, token_symbol, token_price, token_qty, token_lowprice, token_lowqty, reg_date, end_date, token_proceed, buysell_top, buysell_subject, buysell_content, user_idx, nickname, seller_tel, order_no) ";
    qStr += "values('0', '"+token_idx+"', '"+token_symbol+"', "+token_price+", "+parseFloat(token_qty)+", "+token_lowprice+", "+parseFloat(token_lowqty)+", now(), date_add(now(), interval +"+doDate+" day), '"+token_proceed+"', "+buysell_top+", '"+buysell_subject+"', '"+buysell_content+"', "+user_idx+", '"+nickname+"', '"+seller_tel+"', '"+order_no+"') ";

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const name = req.body.name || '';
  const recommender = req.body.recommender || '';
  const contact = req.body.contact || '';
  const pwd = req.body.pwd || '';
  const sId = req.body.sId || '';
  sId_back = sId;
  const email = req.body.email || '';
  const c_key = req.body.c_key || '';
  const pay_point = parseInt(req.body.pay_point, 10) || '0';
  const free_point = parseInt(req.body.free_point, 10) || '0';
  const bank_adress = req.body.worktime || '';
  const jijum_contact = req.body.jijum_contact || '';
  const type = req.body.type || '3';

  models.buysell.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((user) => {
    models.User.update(
      {
        name: name ? name : User.name,
        recommender: recommender ? recommender : User.recommender,
        contact: contact ? contact : User.contact,
        pwd: pwd ? pwd : User.contact,
        sId: sId ? sId : User.sId,
        email: email ? email : User.email,
        c_key: c_key ? c_key : User.c_key,
        pay_point: pay_point ? pay_point : User.pay_point,
        free_point: free_point ? free_point : User.pay_point,
        bank_adress: bank_adress ? bank_add : User.bank_adress,
        jijum_contact: jijum_contact ? jijum_contact : User.jijum_contact,
        type: type
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((user) => res.status(200).json(user))
  })    
  }

  