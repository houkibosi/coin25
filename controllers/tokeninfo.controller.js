const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
  var qStr = "";
  qStr += "select idx, token_krname, token_symbol from token_infotbl;";

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(data => res.json(
    {
      'data' : data
    }
  ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const idxCoin = parseInt(req.params.id, 10);
  if (!idxCoin) {
    return res.status(400).json({error: 'Incorrect idxCoin'});
  }

  var qStr = "";
  qStr += "select * from token_infotbl where idx = "+idxCoin+";";

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.buysell.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const buysell_div = req.body.buysell_div || '';
    const user_idx = req.body.user_idx || '';  
    const name = req.body.name || '';
    const nickname = req.body.nickname || '';
    const token_idx = parseInt(req.body.token_idx, 10) || '0';
    const token_symbol = req.body.token_symbol || '';
    const token_price = parseInt(req.body.token_price, 10) || '0';
    const token_qty = req.body.token_qty || '';
    const token_lowprice = parseInt(req.body.token_lowprice, 10) || '0';
    const token_lowqty = req.body.token_lowqty || '';
    const reg_date = req.body.reg_date || false;
    const end_date = req.body.end_date || '';
    const token_proceed = req.body.token_proceed || '';
    const buysell_top = req.body.buysell_top || '';
    const buysell_subject = req.body.buysell_subject || '';
    const buysell_content = req.body.buysell_content || '';
    
    models.buysell.create({
      token_krname: token_krname,
      token_enname: token_enname,
      token_symbol: token_symbol,
      token_listed: token_listed,
      token_logo_path: token_logo_path,
      token_field: token_field,
      token_amount: token_amount,
      token_hardcap: token_hardcap,
      token_softcap: token_softcap,
      token_price: token_price,
      token_whitepaperUrl: token_whitepaperUrl,
      token_site: token_site,
      token_youtube: token_youtube,
      token_facebook: token_facebook,
      token_twitter: token_twitter,
      token_linkedin: token_linkedin,
      token_medium: token_medium,
      token_telegram_kr: token_telegram_kr,
      token_telegram_en: token_telegram_en,
      token_kakao: token_kakao,
      token_steemit: token_steemit,
      token_kyc: token_kyc,
      ico_started: ico_started,
      ico_end: ico_end,
      ico_proceed: ico_proceed,
      token_tel: token_tel,
      token_address: token_address,
      token_email: token_email,
      token_ceo: token_ceo,
      ico_check: ico_check
    })
    .then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const name = req.body.name || '';
  const recommender = req.body.recommender || '';
  const contact = req.body.contact || '';
  const pwd = req.body.pwd || '';
  const sId = req.body.sId || '';
  sId_back = sId;
  const email = req.body.email || '';
  const c_key = req.body.c_key || '';
  const pay_point = parseInt(req.body.pay_point, 10) || '0';
  const free_point = parseInt(req.body.free_point, 10) || '0';
  const bank_adress = req.body.worktime || '';
  const jijum_contact = req.body.jijum_contact || '';
  const type = req.body.type || '3';

  models.buysell.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((user) => {
    models.User.update(
      {
        name: name ? name : User.name,
        recommender: recommender ? recommender : User.recommender,
        contact: contact ? contact : User.contact,
        pwd: pwd ? pwd : User.contact,
        sId: sId ? sId : User.sId,
        email: email ? email : User.email,
        c_key: c_key ? c_key : User.c_key,
        pay_point: pay_point ? pay_point : User.pay_point,
        free_point: free_point ? free_point : User.pay_point,
        bank_adress: bank_adress ? bank_add : User.bank_adress,
        jijum_contact: jijum_contact ? jijum_contact : User.jijum_contact,
        type: type
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((user) => res.status(200).json(user))
  })    
  }

  