const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
    models.User.findAll({
      // where : {
      //   bDelete: {
      //     [Op.ne]: 1
      //   }
      // }
    })
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const sId = parseInt(req.params.sId, 10);
  if (!sId) {
    return res.status(400).json({error: 'Incorrect sId'});
  }

  models.User.findOne({
    where: {
      sId: sId
    }
  }).then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.User.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const email = req.body.email || '';
    const type = req.body.type || '3';  
    const name = req.body.name || '';
    const nickname = req.body.nickname || '';
    const recommender = req.body.recommender || '';
    const jijum_contact = req.body.jijum_contact || '';
    const contact = req.body.contact || '';
    const pwd = req.body.pwd || '';
    const pay_point = parseInt(req.body.pay_point, 10) || '0';
    const free_point = parseInt(req.body.admin, 10) || '0';
    const b_delete = req.body.b_delete || false;
    const bank_code = req.body.bank_code || '';
    const account_no = req.body.account_noaccount_no || '';
    const partner_code = req.body.partner_code || '';
    const flag = req.body.flag || '';
    
    var userIndex=0;

    if (!email) {
      return res.status(400).json({error: 'Incorrect email'});
    }

    models.User.create({
      email: email,
      type: type,
      name: name,
      nickname: nickname,
      recommender: recommender,
      jijum_contact: jijum_contact,
      contact: contact,
      pwd: pwd,
      pay_point: pay_point,
      free_point: free_point,
      b_delete: b_delete,
      bank_code: bank_code,
      account_no: account_no,
      partner_code: partner_code,
      flag: flag
    })
    .then((user) => res.status(201).json(user))
    .then((user)=> {

      // 가입자 메일 정보로 방금 가입한 정보 조회
      var qStr = "";
      qStr += "select idx from users_mastertbls where email = '" + email + "'";
      models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
      .then((data) => {
        console.log(data);

        // 가입자에게 가입축하 포인트 지급
        userIndex = data[0].idx;
        var qStr = "";
        qStr += "insert into point_mastertbls (user_idx, email, state_div, used_point, point_detail, recommend_name, recommend_email, reg_date) ";
        qStr += "values ("+userIndex+", '"+email+"', '리워드', 10000, '최초 가입 포인트 리워드', '코인25', 'admin@coin25.co', now())";
        models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
      })
      .then((data)=>{

        // 추천인이 있을경우
        console.log("추천인이 있을경우")
        if(!recommender){
          console.log("추천인이 없습니다.");
        } else {

          // 추천인 연락처로 조회
          var qStr = "";
          qStr += "SELECT idx, email, name, partner_code, contact FROM users_mastertbls where contact = '" + recommender + "'";
          models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
          .then((data)=>{
            // 가입인에게 추천 포인트 지급
            var qStr = "";
            qStr += "insert into point_mastertbls (user_idx, email, state_div, used_point, point_detail, recommend_name, recommend_email, reg_date) ";
            qStr += "values ("+userIndex+", '"+email+"', '리워드', 1000, '추천인 가입 포인트 리워드', '"+data[0].name+"', '"+data[0].email+"', now())";
            models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
          })

          models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
          .then((data)=>{
            // 추천인에게 추천 포인트 지급
            var qStr = "";
            qStr += "insert into point_mastertbls (user_idx, email, state_div, used_point, point_detail, recommend_name, recommend_email, reg_date) ";
            qStr += "values ("+data[0].idx+", '"+data[0].email+"', '리워드', 2000, '추천인 가입 포인트 리워드', '"+name+"', '"+email+"', now())";
            models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
          })

          models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
          .then((data)=>{
            // 추천인의 파트너코드 가져오기
            var qStr = "";
            qStr += "update users_mastertbls set partner_code = ( ";
            qStr += "select * from  (select partner_code from users_mastertbls where contact = '"+data[0].contact+"') as data" ;
            qStr += ") where email = '"+email+"'";
            models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
          })
        }
      })
        

      
    })
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const name = req.body.name || '';
  const recommender = req.body.recommender || '';
  const contact = req.body.contact || '';
  const pwd = req.body.pwd || '';
  const sId = req.body.sId || '';
  sId_back = sId;
  const email = req.body.email || '';
  const c_key = req.body.c_key || '';
  const pay_point = parseInt(req.body.pay_point, 10) || '0';
  const free_point = parseInt(req.body.admin, 10) || '0';
  const bank_adress = req.body.worktime || '';
  const jijum_contact = req.body.jijum_contact || '';
  const type = req.body.type || '3';

  models.User.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((user) => {
    models.User.update(
      {
        name: name ? name : User.name,
        recommender: recommender ? recommender : User.recommender,
        contact: contact ? contact : User.contact,
        pwd: pwd ? pwd : User.contact,
        sId: sId ? sId : User.sId,
        email: email ? email : User.email,
        c_key: c_key ? c_key : User.c_key,
        pay_point: pay_point ? pay_point : User.pay_point,
        free_point: free_point ? free_point : User.pay_point,
        bank_adress: bank_adress ? bank_add : User.bank_adress,
        jijum_contact: jijum_contact ? jijum_contact : User.jijum_contact,
        type: type
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((user) => res.status(200).json(user))
  })    
  }

  