const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
    models.notice.findAll({
      // where : {
      //   bDelete: {
      //     [Op.ne]: 1
      //   }
      // }
    })
        .then(data => res.json(
          {
            'data' : data
          }
        ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const userid = parseInt(req.params.userid, 10);
  if (!userid) {
    return res.status(400).json({error: 'Incorrect userid'});
  }

  models.notice.findOne({
    where: {
      userid: userid
    }
  }).then(notice => {
    if (!notice) {
      return res.status(404).json({error: 'No notice'});
    }

    return res.json(notice);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const userid = parseInt(req.params.userid, 10);
    if (!userid) {
      return res.status(400).json({error: 'Incorrect userid'});
    }
  
    models.notice.update({
      bDelete: '1'
      }, {
      where: {
        userid: userid
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const title = req.body.title || '';
    const content = req.body.content || '';
    const bDelete = req.body.bDelete || '';
    const writer = req.body.writer || '';
    

    if (!writer) {
      return res.status(400).json({error: 'Incorrect id'});
    }

    models.notice.create({
      title: title,
      content: content,
      bDelete: bDelete,
      writer: writer
    })
    .then((notice) => res.status(201).json(notice))
};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const sIp = body.sIp;
  const name = body.name;
  const phone = body.phone;
  const password = body.password;
  const admin = body.admin;
  const address = body.address;
  const inday = body.inday;
  const outday = body.outday;
  const email = body.email;
  const team = body.team;
  const worktime = parseInt(body.worktime, 10);
  const unuselevel = parseInt(body.unuselevel, 10);

  models.notice.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((notice) => {
    models.notice.update(
      {
        phone: phone? phone : notice.phone,
        password: password? password : notice.password,
        admin: admin? admin : notice.admin,
        worktime: worktime? worktime : notice.worktime,
        inday: inday? inday : notice.inday,
        outday: outday? outday : notice.outday,
        worktime: worktime? worktime : notice.worktime,
        unuselevel: unuselevel? unuselevel : notice.unuselevel,
        email: email? email : notice.email,
        team: team? team : notice.team
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((notice) => res.status(200).json(notice))
  })    
  }

  