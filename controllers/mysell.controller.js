const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;
var sId_back = "";

// 전체 찾기
exports.index = (req, res) => {
  const myIdx = req.params.id;

    var qStr = "";
    // qStr += "select * from buysell_tradetbl";
    // qStr += "select bm.idx, bt.idx, bm.token_symbol, ti.token_krname, ti.token_enname, bm.token_price, bm.token_qty, bm.token_lowqty, bm.token_lowprice, bm.nickname, bt.trade_state ";
    // qStr += "from buysell_tradetbl bt inner join buysell_mastertbl bm on bt.buysell_idx = bm.idx left outer join token_infotbl ti on bm.token_idx = ti.idx ";
    // qStr += "where bt.user_idx = " + myIdx;

    // qStr += "select idx, token_symbol, token_price, token_qty, token_lowqty, token_lowprice, nickname, token_proceed ";
    // qStr += "select * ";
    // qStr += "from buysell_mastertbl where user_idx = " + myIdx;

    qStr += "select bm.idx, bm.order_no, ti.token_krname, ti.token_symbol, bm.token_qty, bm.token_price, bm.token_lowqty, bm.token_lowprice, bm.reg_date, bm.end_date ";
    qStr += "from buysell_mastertbl bm inner join token_infotbl ti on bm.token_idx = ti.idx where bm.user_idx =" + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const buysell_div = req.params.div;
  const id = req.params.id;
  

  // models.buysell.findOne({
  //   where: {
  //     sId: sId
  //   }
  // })
  var qStr = "";
  // qStr += "select * from buysell_tradetbl where buysell_idx = " + id;
  // qStr += "select bm.idx, bm.order_no, bm.nickname, bm. ti.token_krname, ti.token_symbol, bm.token_qty, bm.token_price, bm.token_lowqty, bm.token_lowprice, bm.reg_date, bm.end_date ";
  // qStr += "from buysell_mastertbl bm inner join token_infotbl ti on bm.token_idx = ti.idx where bm.user_idx =" + id;
  qStr += "select bm.idx, bm.order_no, ti.token_krname, ti.token_symbol, bt.token_qty, bt.trade_price, bt.nickname, bt.token_wallet_address, bt.trade_state ";
  qStr += "from buysell_mastertbl bm inner join buysell_tradetbl bt on bm.idx = bt.buysell_idx inner join token_infotbl ti on bm.token_idx = ti.idx ";
  qStr += "where bt.buysell_idx =" + id;

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
  .then(user => {
    if (!user) {
      return res.status(404).json({error: 'No User'});
    }

    return res.json(user);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.buysell.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const buysell_div = req.body.buysell_div || '';
    const token_idx = parseInt(req.body.token_idx, 10) || '0';
    const token_symbol = req.body.token_symbol || '';
    const token_price = parseInt(req.body.token_price, 10) || '0';
    const token_qty = req.body.token_qty || '';
    const token_lowprice = parseInt(req.body.token_lowprice, 10) || '0';
    const token_lowqty = req.body.token_lowqty || '';
    const reg_date = req.body.reg_date || false;
    const end_date = req.body.end_date || '';
    const token_proceed = req.body.token_proceed || '';
    const buysell_top = req.body.buysell_top || '';
    const buysell_subject = req.body.buysell_subject || '';
    const buysell_content = req.body.buysell_content || '';

    const user_idx = parseInt(req.body.user_idx) || '';
    const nickname = req.body.nickname || '';
    
    // models.buysell.create({      
    //   buysell_div: buysell_div,
    //   token_idx: token_idx,
    //   token_symbol: token_symbol,
    //   token_price: token_price,
    //   token_qty: token_qty,
    //   token_lowprice: token_lowprice,
    //   token_lowqty: token_lowqty,
    //   reg_date: reg_date,
    //   end_date: end_date,
    //   token_proceed: token_proceed,
    //   buysell_top: buysell_top,
    //   buysell_subject: buysell_subject,
    //   buysell_content: buysell_content,

    //   user_id: user_idx,
    //   nickname: nickname
    // })
    // str += "select a.email as email, b.state_div as state_div, b.used_point as used_point, b.point_detail as point_detail, DATE_FORMAT(b.reg_date, '%Y.%c.%d %H:%i') as createdAt, substring(b.recommend_name,1,2) as recommend_name ";
    // str += "from users_mastertbls a left outer join point_mastertbls b on a.email = b.email where contact like '"+phone+"' order by createdAt desc";
    var qStr = "";
    qStr += "insert into buysell_tradetbl (buysell_div, token_idx, token_symbol, token_price, token_qty, token_lowprice, token_lowqty, reg_date, end_date, token_proceed, buysell_top, buysell_subject, buysell_content, user_idx, nickname) ";
    qStr += "values('0', '"+token_idx+"', '"+token_symbol+"', "+token_price+", "+token_qty+", "+token_lowprice+", "+token_lowqty+", '"+reg_date+"', '"+end_date+"', '"+token_proceed+"', "+buysell_top+", '"+buysell_subject+"', '"+buysell_content+"', "+user_idx+", '"+nickname+"') ";

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then((user) => res.status(201).json(user))
};

// 업데이트
exports.update = (req, res) => {
  const idx = req.body.idx;
  const trade_state = req.body.trade_state;

  var qStr = "";
  qStr += "update buysell_tradetbl set trade_state = '"+trade_state+"' where idx = "+idx;

  models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})  
  .then((user) => res.status(200).json(user))    
  }

  