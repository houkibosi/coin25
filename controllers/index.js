const express = require('express');
const expressSession =  require('express-session');
const router = express.Router();
const auth = require('../auth/auth');

const fs = require('fs');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const mime = require('mime-types');
const path = require('path');

const iconvLite = require('iconv-lite');

const multer = require('multer');
const _storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/public/uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
const upload = multer({storage: _storage});

const models = require('../models/models');


// const upload = multer({
//     storage: multer.diskStorage({
//       destination: function (req, file, cb) {
//         cb(null, 'd:\\');
//       },
//       filename: function (req, file, cb) {
//         cb(null, file.originalname);
//       }
//     }),
//   });
// const  multiparty = require('multiparty');

const formidable = require('formidable');


// main Pages
// router.post('/', passport.authenticate('local',
//     function(req, res) {
//         successRedirect: '/index'
//     }
// ));
// router.route('/').post(passport.authenticate('local'),
//     function(req, res){
//         res.redirect('/' + req.user.username);
//     }
// );

router.get('/', (req, res) => {
    res.render('index.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/login', (req, res) => {
    res.render('login.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/home', isLoggedin, (req, res) => {
    res.render('home.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/sell_list', isLoggedin, (req, res) => {
    res.render('sell_list.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/sell_list_detail', isLoggedin, (req, res) => {
    res.render('sell_list_detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/sell_request', isLoggedin, (req, res) => {
    res.render('sell_request.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/buy_list', isLoggedin, (req, res) => {
    res.render('buy_list.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/buy_request', isLoggedin, (req, res) => {
    res.render('buy_request.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/auction_list', isLoggedin, (req, res) => {
    res.render('auction_list.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/auction_request', isLoggedin, (req, res) => {
    res.render('auction_request.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/coin_community_detail', isLoggedin, (req, res) => {
    res.render('coin_community_detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/coin_community_write', isLoggedin, (req, res) => {
    res.render('coin_community_write.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/coin_community', isLoggedin, (req, res) => {
    res.render('coin_community.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/faq', isLoggedin, (req, res) => {
    res.render('faq.ejs', {
        title : 'Express',

        user: req.user
    });
});

router.get('/my_point', isLoggedin, (req, res) => {
    res.render('my_point.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_buysell', isLoggedin, (req, res) => {
    res.render('my_buysell.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_buy', isLoggedin, (req, res) => {
    res.render('my_buy.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_sell', isLoggedin, (req, res) => {
    res.render('my_sell.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_buysell_detail', isLoggedin, (req, res) => {
    res.render('my_buysell_detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_buysell2', isLoggedin, (req, res) => {
    res.render('my_buysell2.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_account', isLoggedin, (req, res) => {
    res.render('my_account.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/my_wallet', isLoggedin, (req, res) => {
    res.render('my_wallet.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/notice_detail', isLoggedin, (req, res) => {
    res.render('notice_detail.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/notice', isLoggedin, (req, res) => {
    res.render('notice.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/privacy', (req, res) => {
    res.render('privacy.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/profile', isLoggedin, (req, res) => {
    res.render('profile.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/terms', isLoggedin, (req, res) => {
    res.render('terms.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/recoverypw1', isLoggedin, (req, res) => {
    res.render('recoverypw1.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/recoverypw2', isLoggedin, (req, res) => {
    res.render('recoverypw2.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/register', (req, res) => {
    res.render('register.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/auction_list', isLoggedin, (req, res) => {
    res.render('auction_list.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/coin_list', isLoggedin, (req, res) => {
    res.render('coin_list.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/newcoin', isLoggedin, (req, res) => {
    res.render('newcoin.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/guide', isLoggedin, (req, res) => {
    res.render('guide.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/question', isLoggedin, (req, res) => {
    res.render('question.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/proposal', isLoggedin, (req, res) => {
    res.render('proposal.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/uk92hr094jhojhks98045thiuoerg98u0thkuj89eoui3079ijer09u5oiu', (req, res) => {
    res.render('monitor.ejs', {
        title : 'Express',
        user: req.user
    });
});

router.get('/0983t54qjoit50u9tq40u9qgjioq3t49u80teqhiotau90q3ti', (req, res) => {
    res.render('notice_write.ejs', {
        title : 'Express',
        user: req.user
    });
});



// User API
const user = require('./user.controller');
router.get('/users', user.index);
router.get('/users/:companyid', user.show);
router.delete('/users/:id', user.destroy);
router.post('/users', user.create);
router.put('/users/:id', user.update);

// point API
const point = require('./point.controller');
router.get('/points', point.index);
router.post('/mypoints', point.show);
router.delete('/points/:id', point.destroy);
router.post('/points', point.create);
router.put('/points/:id', point.update);

// // notice API
const notice = require('./notice.controller');
router.get('/notices', notice.index);
router.get('/notices/:id', notice.show);
router.delete('/notices/:id', notice.destroy);
router.post('/notices', notice.create);
router.put('/notices/:id', notice.update);


// 신규 추가 된 테이블
// 베이직 API
const basic = require('./basic.controller');
router.get('/basics', basic.index);
router.get('/basics/:id', basic.show);
router.delete('/basics/:id', basic.destroy);
router.post('/basics', basic.create);
router.put('/basics/:id', basic.update);

// 구매판매 API
const buysell = require('./buysell.controller');
router.get('/buysells/:buysell_div', buysell.index);
router.get('/buysells/:buysell_div/:id', buysell.show);
router.delete('/buysells/:id', buysell.destroy);
router.post('/buysells', buysell.create);
router.put('/buysells/:id', buysell.update);

// 구매판매 거래자 API
const buysell_trade = require('./buysell_trade.controller');
router.get('/buysell_trade/:buysell_div', buysell_trade.index);
router.get('/buysell_trade/:buysell_div/:id', buysell_trade.show);
router.delete('/buysell_trade/:id', buysell_trade.destroy);
router.post('/buysell_trade', buysell_trade.create);
router.put('/buysell_trade/:id', buysell_trade.update);


// 나의 판매 거래 API
const mysell = require('./mysell.controller');
router.get('/mysell/:id', mysell.index);
router.get('/mysell/:div/:id', mysell.show);
router.delete('/mysell/:id', mysell.destroy);
router.post('/mysell', mysell.create);
router.put('/mysell/:id', mysell.update);

// 나의 판매 거래 모달 API
// const myselld = require('./mysell_detail.controller');
// router.get('/myselld/:id', myselld.index);
// router.get('/myselld/:div/:id', myselld.show);
// router.delete('/myselld/:id', myselld.destroy);
// router.post('/myselld', myselld.create);
// router.put('/myselld/:id', myselld.update);

// 나의 구매 거래 API
const mybuy = require('./mybuy.controller');
router.get('/mybuy/:id', mybuy.index);
router.get('/mybuy/:div/:id', mybuy.show);
router.delete('/mybuy/:id', mybuy.destroy);
router.post('/mybuy', mybuy.create);
router.put('/mybuy/:id', mybuy.update);


// 토큰정보 API
const tokeninfo = require('./tokeninfo.controller');
router.get('/tokeninfos', tokeninfo.index);
router.get('/tokeninfos/:id', tokeninfo.show);
router.delete('/tokeninfos/:id', tokeninfo.destroy);
router.post('/tokeninfos', tokeninfo.create);
router.put('/tokeninfos/:id', tokeninfo.update);

// 토큰정보 API
const mywallet = require('./mywallet.controller');
router.get('/mywallets/:id', mywallet.index);
router.get('/mywallets/:id/:wid', mywallet.show);
router.delete('/mywallets/:id', mywallet.destroy);
router.post('/mywallets', mywallet.create);
router.put('/mywallets/:id', mywallet.update);





router.post('/getAccountInfo', function(req, res){    
    const user_id = req.body.id;
    if(!user_id)
    {
        return res.status(400).json({error: 'Incorrect User index'});
    }

    var queryStr = "";
    queryStr += "SELECT bank_code, account_no FROM users_mastertbls where idx = " + user_id;

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => {
        if (!data) {
        return res.status(404).json({error: 'No data'});
        }

        //console.log(data);
        return res.json(data);
    });
});

router.post('/updateAccount', function(req, res){
    const userid = req.body.userid;
    const bank_code = req.body.bank_code;
    const account = req.body.account;
    if (!account) {
        return res.status(400).json({error: 'Incorrect account'});
    }

    var queryStr = "";
    queryStr += "UPDATE users_mastertbls SET bank_code = '"+bank_code+"', account_no = '"+account+"' where idx = "+userid+";";

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(Telnum => {
        if (!Telnum) {
        return res.status(404).json({error: 'No Telnum'});
        }

        //console.log(Telnum);
        return res.json(Telnum);
    });
});

router.get('/getListBankCode', function(req, res){    
    var queryStr = "";
    queryStr += "SELECT * FROM bank_infotbls;";

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(Telnum => {
        if (!Telnum) {
        return res.status(404).json({error: 'No Telnum'});
        }

        //console.log(Telnum);
        return res.json(Telnum);
    });
});


router.post('/checkMember', function(req, res){
    const telnum = req.body.telnum;
    if (!telnum) {
        return res.status(400).json({error: 'Incorrect sId'});
    }

    
    var queryStr = "";
    queryStr += "SELECT type, recommender, jijum_contact FROM list_reservation where contact = '"+telnum+"'";

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(Telnum => {
        if (!Telnum) {
        return res.status(404).json({error: 'No Telnum'});
        }

        //console.log(Telnum);
        return res.json(Telnum);
    });
});

router.post('/checkMember2', function(req, res){
    //console.log("checkMember2");
    const telnum = req.body.telnum;
    if (!telnum) {
        return res.status(400).json({error: 'Incorrect sId'});
    }

    var queryStr = "";
    queryStr += "SELECT sId, contact, jijum_contact, type FROM users_mastertbls where contact = '"+telnum+"'";

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(Telnum => {
        if (!Telnum) {
        return res.status(404).json({error: 'No Telnum', sId:''});
        }

        var jijum_contact="", mypoint="", friendpoint=""; 
        if(Telnum.type == 1)
        {
            
        }

        var queryStr2 = "";
        if(Telnum.lenght > 0){
            
        }

        return res.json(Telnum);
    });
});

router.post('/checkReceivePoint', function(req, res) {
 
    var phone = req.body.phone;
   // var phone2 = $.trim(phone);

    if (!phone) {
        return res.status(400).json({error: 'Incorrect phone'});
    }

    var queryStr = "";
    queryStr += "SELECT name, email FROM users_mastertbls where contact = '"+phone+"';";

    //console.log("checkReceivePoint: "+ queryStr);

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(user => {
        if (!user) {
            return res.status(404).json({error: 'No User'});
        }
        //console.log("checkReceivePoint:" + user.name);
        return res.json(user);
    });
});

router.post('/email2phone', function(req, res){
    const email = req.body.email;
    if (!email) {
        return res.status(400).json({error: 'Incorrect sId'});
    }

    queryStr += "SELECT contact FROM users_mastertbls where sid = '" + email + "'";

    models.sequelize.query(queryStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => {
            if (!Data) {
            return res.status(404).json({error: 'No Data'});
            }

        return res.json(Data);
    });
});

function eventAddPoint(){ 
    var sId = Telnum.sId;
    var contact = Temnum.contact;
    var queryStr2 = ""
    queryStr2 += "select * from point_mastertbls where sId ='" +sId+ "' order by updatedAt desc LIMIT 1";

    models.sequelize.query(queryStr2, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => 
    {
        if(Data.length == 0)
        {
            models.Point.create(
            {
                sId: sId,
                sState: "리워드",
                nUsedPoint: "10000",
                nRemainPoint: "10000",
                sDetail: "사전가입 추천인 이벤트 보너스",
                bDelete: false
            })
        } else {
            models.Point.create({
                sId: sId,
                sState: "리워드",
                nUsedPoint: "10000",
                nRemainPoint: parseInt(Data.nRemainPoint) + 10000,
                sDetail: "사전가입 추천인 이벤트 보너스",
                bDelete: false
            })
        }
    });
}


// login API
const login = require('./login.controller');
//router.post('/login', passport.authenticate('local', {successRedirect:'/index', failureRedirect: '/'}));
// router.post('/login', passport.authenticate('local-login', {
//     successRedirect: '/my_point',
//     failureRedirect: '/login'
// }));

router.post('/login' , 
    passport.authenticate('local-login', { 
        failureRedirect: '/login', 
        failureFlash: true 
    }), 
    function(req, res){
        // res.send('<script>alert("로그인 성공");location.href="/my_point";</script>');
        res.send("success");
    }
);

router.put('/login/:id', login.update);
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});



// 인증 관리
function isLoggedin (req, res, next) 
{
    // console.log(req.isAuthenticated());
    // console.log(req.user);
    if (req.isAuthenticated())
    {
        return next();
    } 
    else 
    {
        res.redirect('/login');
    }
}


// export
module.exports = router;



router.post('/check', function(req, res){
    console.log("/check");
    var phone = req.body.phone;
    console.log("phone = " + phone);
    var key = Math.floor(Math.random()*(9999-1000+1)) + 1000;
    console.log("key = " + key);
    var Coolsms = require('coolsms-rest-sdk');
    var client = new Coolsms({
    key: 'NCS5UQXRAEZ4BJ48',
    secret: 'AYR8FLPJ9LABONTLVY2QWKWL4IKWKDGR'
    });
    
    client.sms.send({
        to: phone,    // recipient
        from: '15881847',  // sender
        type: 'SMS',          // SMS, LMS, MMS
        text: '[COIN25 인증번호] ' + key,
        }, function (error, result) {
        if(error) {
            return res.json(error);
        } else {
            req.session.key = key;
            return res.json(key);
        }
    });
})

router.post('/joinok', function(req, res){
    var phone = req.body.phone;
    // console.log("phone = " + phone);
    // var key = Math.floor(Math.random()*(9999-1000+1)) + 1000;
    // console.log("key = " + key);
    var Coolsms = require('coolsms-rest-sdk');
    var client = new Coolsms({
    key: 'NCS5UQXRAEZ4BJ48',
    secret: 'AYR8FLPJ9LABONTLVY2QWKWL4IKWKDGR'
    });
    
    client.sms.send({
    to: phone,    // recipient
    from: '15881847',  // sender
    type: 'SMS',          // SMS, LMS, MMS
    text: '[COIN25] 코인25에 가입해주셔서 감사합니다.',
    }, function (error, result) {
    if(error) {
        return res.json(error);
    } else {
        return res.json("ok");
    }
    });
})

// 회원에게 진행관련 문자 전송
router.post('/sendlms', function(req, res){
    // 문자 내용을 구분 할 상태코드
    var state = parseInt(req.body.state, 10) || 0;
    if (!state) {
        return res.status(400).json({error: 'Incorrect State'});
    }

    // 거래 번호
    var dealNumber = req.body.dealNumber || '';
    // 거래 가격
    var dealPrice = req.body.dealPrice || '';
    // 거래 코인
    var dealCoin = req.body.dealCoin || '';
    // 거래 수량
    var dealCount = req.body.dealCount || '';

    // 판매자 이름
    var sellerName = req.body.sellerName || '';
    // 판매자 연락처
    var sellerPhone = req.body.sellerPhone || '';
    // 판매자 은행
    var sellerBank = req.body.sellerBank || '';
    // 판매자 계좌번호
    var sellerAccount = req.body.sellerAccount || '';
    // 판매자 지갑주소
    var sellerWaddress = req.body.sellerWaddress || '';
    
    // 구매자 이름
    var buyerName = req.body.buyerName || '';
    // 구매자 연락처
    var buyerPhone = req.body.buyerPhone || '';
    // 판매자 은행
    var buyerBank = req.body.buyerBank || '';
    // 구매자 계좌번호
    var buyerAccount = req.body.buyerAccount || '';
    // 구매자 지갑주소
    var buyerWaddress = req.body.buyerWaddress || '';

    // 문자발송 관련 변수
    var smsReception = "";
    var smsSubject = "";
    var smsContent = "";

    // state로 상태 별 분기
    switch(state){
        case 1 : 
            smsReception = "'" + sellerPhone + "'";
            smsSubject = "[COIN25] 구매거래요청";
            smsContent += "판매등록하신  "+dealCoin+"의 구매거래요청이 있습니다.\n나의판매정보에서 거래승인여부를 확인해주세요.\n\n";
            smsContent += "거래번호 : "+dealNumber+"\n구매자 : "+buyerName+"\n코인명 : "+dealCoin+"\n거래수량 : "+dealCount+" 개\n거래금액 : "+dealPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',')+" 원";
        break;
        case 2 : 
            smsReception = "'" + buyerPhone + "'";
            smsSubject = "[COIN25] 구매승인 및 입금요청";
            smsContent += "구매요청하신 거래가 승인되었습니다.\nCOIN25 계좌로 구매대금을 입금하시고, 나의 구매정보에서 입금확인을 해주세요.\n";
            smsContent += "거래번호 : "+dealNumber+"\n판매자 : "+sellerName+"\n코인명 : "+dealCoin+"\n거래수량 :  "+dealCount+" 개\n";
            smsContent += "입금자명 : **********\n은행명 : "+buyerBank+"\n계좌번호 : "+buyerAccount+"\n거래금액 :  "+dealPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',')+"원\n\n";
            smsContent += "등록한 계좌에서 COIN25 거래계좌로 거래금액을 입금해주세요.\n";
            smsContent += "-------------\n";
            smsContent += "예금주 : 주식회사 엠엔유\n은행 : IBK 기업은행\n입금계좌 : 078-195044-01-027\n";
        break;
        case 3 : 
            smsReception = "'" + buyerPhone + "'";
            smsSubject = "[COIN25] 거래취소";
            smsContent += "구매요청하신 거래를 판매자가 취소하셨습니다. 다른 판매자에게 거래요청 하시기 바랍니다.";
        break;
        case 4 : 
            smsReception = "'" + sellerPhone + "'";
            smsSubject = "[COIN25] 구매자코인전송요청";
            smsContent += "판매등록하신  "+dealCoin+"의 판매대금이 COIN25에 입금되었습니다. 구매자의 지갑주소로 코인을 전송하시고, 나의 판매정보에서 코인전송완료 확인을 해주세요.\n";
            smsContent += "거래번호 : "+dealNumber+"\n구매자 : "+buyerName+"\n코인명 : "+dealCoin+"\n거래수량 :  "+dealCount+" 개\n구매자지갑주소 : "+buyerWaddress+"\n\n";
            smsContent += "코인전송수수료는 판매자가 부담합니다.";
        break;
        case 5 : 
            smsReception = "'" + buyerPhone + "'";
            smsSubject = "[COIN25] 구매코인지갑전송확인요청";
            smsContent += "구매요청하신 "+dealCoin+"을 판매자가 지갑으로 전송하였습니다. 등록하신 지갑으로  코인이 전송되었는지 확인하시고, 나의 구매정보에서 코인전송 확인을 해주세요.\n";
            smsContent += "거래번호 : +"+dealNumber+"+\n판매자 : "+sellerName+"\n코인명 : "+dealNumber+"\n거래수량 :  "+dealCount+" 개\n판매자지갑주소 : "+sellerWaddress + "\n\n";
            smsContent += "판매자지갑주소를 확인하세요.";
        break;
        case 6 : 
            smsReception = "'" + sellerPhone + "'";
            smsSubject = "[COIN25] 판매대금입금";
            smsContent += "판매등록하신  "+dealCoin+"의 판매대금을 등록하신 계좌로 입금을 하였습니다. 입금금액을 확인하시고, 판매정보에서 입금확인을 해주세요.";
            var fee = parseInt(dealPrice) * 0.01;
            smsContent += "거래번호 : "+dealNumber+"\n판매자 : "+sellerName+"\n코인명 : "+dealNumber+"n거래수량 :  "+dealCount+" 개\n거래금액 : "+dealPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ',')+"원\n거래수수료 :  "+fee+"원(1%)\n";
        break;
    }

    // COOL SMS 인증 및 객체
    var Coolsms = require('coolsms-rest-sdk');
    var client = new Coolsms({
        key: 'NCS5UQXRAEZ4BJ48',
        secret: 'AYR8FLPJ9LABONTLVY2QWKWL4IKWKDGR'
    });

    // var content = "";
    // content+="[COIN25] 구매승인 및 입금요청 \n\n 구매요청하신 거래가 승인되었습니다\nCOIN25 계좌로 구매대금을 입금하시고, 나의 구매정보에서 입금확인을 해주세요.\n\n거래번호:556625634\n판매자 : 닉네임\n코인명 :비트코인\n";
    // content+="거래수량 : 3개\n\n입금자명 : : **********\n은행명 : **********\n계좌번호 : **********\n거래금액 :  *********원\n\n등록한 계좌에서 COIN25 거래계좌로 거래금액을 입금해주세요.\n------------------------------\n";
    // content+="예금주 : 주식회사 엠엔유\n은행 : IBK 기업은행\n입금계좌 : 078-195044-01-027";
    
    console.log("수신인 : " + smsReception);
    console.log("제목 : " + smsSubject);
    console.log("내용 : " + smsContent);

    client.sms.send({
    to: smsReception,    // recipient
    from: '15881847',  // sender
    type: 'LMS',          // SMS, LMS, MMS
    subject: smsSubject,
    text: smsContent,
    }, function (error, result) {
    if(error) {
        // return res.json(error);
    } else {
        // return res.json("ok");
    }
    });
})



router.post('/checkjoinagain', function(req, res){
//    var sId = req.body.email;
   var phone = req.body.phone;

    // var str = "select count(*) as count from users_mastertbls where sId = '" +sId+ "' or contact = '" + phone + "'";
    var str = "select count(*) as count from users_mastertbls where contact = '" + phone + "'";
   models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
   .then(Data => 
    {
        return res.json(Data);
   });
})

router.post('/sendalert', function(req, res){
    var phone = req.body.phone;
    var message = req.body.message;
    // console.log("phone = " + phone);
    // var key = Math.floor(Math.random()*(9999-1000+1)) + 1000;
    // console.log("key = " + key);
    var Coolsms = require('coolsms-rest-sdk');
    var client = new Coolsms({
        key: 'NCS5UQXRAEZ4BJ48',
        secret: 'AYR8FLPJ9LABONTLVY2QWKWL4IKWKDGR'
    });
    
    client.sms.send({
        to: phone,    // recipient
        from: '15881847',  // sender
        type: 'SMS',          // SMS, LMS, MMS
        text: '[COIN25] ' + message,
        }, function (error, result) {
        if(error) {
            return res.json(error);
        } else {
            return res.json("ok");
        }
    });
})



router.post('/logRecommenderPoint', function(req, res){
    var phone = req.body.contact;
    var sState = req.body.sState;
    var nUsedPoint = req.body.nUsedPoint || 0;
    var nRemainPoint = req.body.nRemainPoint || 0;
    var sDetail = req.body.sDetail;
    var followerName = req.body.followerName || ''; 
    var followerMail = req.body.followerMail || ''; 

     var str = "select sId from users_mastertbls where contact = '" + phone + "'";
     console.log(str);
    models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => 
     {
        
       // console.log("point create sId : " + Data[0].sId);

        if(Data[0].lenght == 0) {

            console.log("point create sId : 0");
        } else {
            models.Point.create({
                sId: Data[0].sId,
                sState: sState,
                nUsedPoint: nUsedPoint,
                nRemainPoint: parseInt(nRemainPoint || 0) + parseInt(nUsedPoint || 0),
                sDetail: sDetail,
                bDelete: 0,
                followerName: followerName,
                followerMail: followerMail
            });
        }



    })
})

router.post('/getmypoint', function(req, res){
    var user_idx = req.body.user_idx;
 
    //var str = "select a.nRemainPoint as nRemainPoint from point_mastertbls a, users_mastertbls b where b.sid = '" + email + "' order by a.createdAt desc limit 1;";
    var str = " select sum(used_point) as nRemainPoint from point_mastertbls where user_idx = "+user_idx+";";

    
    models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => 
     {
        return res.json(Data);
    })
})

router.post('/l893iuhf08uo90ojiq349uferknlnkjzkljg98hkj309uoi30934jkhlsdfg0u89tekhjg09u', function(req, res){
    var phone = req.body.phone || '';
    var name = req.body.name || '';
 
    // var str = "select sId, name, contact, pwd, recommender from users_mastertbls where contact like '%"+phone+"%';";
    var str = "";
    // str += "SELECT um.sId as email, um.name as name, ";
    // str += "concat(um.recommender, ";
    // str += "concat('__', (select name from users_mastertbls where contact = um.contact)) ";
    // str += ") as recommender, ";
    // str += "um.contact as contact, ";
    // str += "um.pwd as pwd, um.createdAt as createdAt ";
    // str += "FROM list_reservation lr ";
    // str += "right outer join users_mastertbls um ";
    // str += "on lr.contact = um.contact ";
    // str += "where 1=1 ";
    // if(name != '')
    //     str += "and um.name = '"+name+"' ";
    // if(phone != '')
    //     str += "and um.contact like '%"+phone+"%'";
    // str += ";";

    str += "SELECT um.sId as email, if(lr.no = '', 'N', 'Y') as prejoin,um.name as name, um.recommender ";
    str += "as recommender, um.pwd as pwd, um.createdAt as createdAt ";
    str += "FROM list_reservation lr right outer join users_mastertbls um on lr.contact = um.contact ";
    str += "where 1=1 ";
    if(name != '')
        str += "and um.name = '"+name+"' ";
    if(phone != '')
        str += "and um.contact like '%"+phone+"%'";
    str += ";";

    
    models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => 
     {
        return res.json(Data);
    })
})

router.post('/oijagf89fu589wqhtg8o7qwhq98547ht98qhtwothbq4ohauioehgfouiqhglqiruej', function(req, res){
    var title = req.body.title;
    var content = req.body.content;
 
    var str = "select sId, recommender, contact, pwd from users_mastertbls where contact = "+phone+";";

    
    models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
    .then(Data => 
     {
        return res.json(Data);
    })
})

router.post('/chgPwssword', function(req, res){
    const phone = req.body.phone;
    const pwd = req.body.pwd;
 
    models.User.findOne({
        where: {
            contact: phone
        }
    }).then((user) => {
        models.User.update(
        {
            pwd: pwd ? pwd : user.contact            
        }, 
        {
          where: 
          {
            contact:phone
          }
        }).then((user) => res.status(200).json(user))
    })
})





// temp
// 판매 - 구매 승인
router.get('/sellcheck/:id', function(req, res){
    const myIdx = req.params.id || '';
    console.log(myIdx);

    var qStr = "";
    qStr += "select * from buysell_mastertbl a, buysell_tradetbl b where a.user_idx = "+myIdx+" and b.trade_state = '구매신청'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });

  // 판매 - 발송 확인
router.get('/sellcheck2/:id', function(req, res){
    const myIdx = req.params.id || '';
    console.log(myIdx);

    var qStr = "";
    qStr += "select * from buysell_tradetbl where user_idx = "+myIdx+" and trade_state = '구매신청'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    )) 
  });

// 상태 업데이트
  router.post('/dealok', function(req, res){
    const myIdx = req.body.idx || '';
    console.log(myIdx);

    var qStr = "";
    // qStr += "update buysell_tradetbl set trade_state = '거래승인' where idx = " + myIdx;
    // 관리자를 거치치 않음
    qStr += "update buysell_tradetbl set trade_state = '입금요청' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ))
    })

//   입금 요청
  router.post('/calldeposit', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '입금요청' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));    
  })    ;

  //   거래 거절
  router.post('/nodeal', function(req, res){
    const myIdx = parseInt(req.body.idx) || 0;

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '거래거절' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));    
  })    ;

//  입금 확인
  router.post('/depositok', function(req, res){
    const myIdx = parseInt(req.body.idx) || 0;
    const wAddress = req.body.wAddress || '';
    const wIndex = parseInt(req.body.wIndex) || 0;

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '입금확인', token_wallet_address = '"+wAddress+"', mywallet_idx = "+wIndex+" where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));
  })    ;

  //  코인 요청
  router.post('/callSendToken', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '토큰전송요청' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));
  })    ;

//  코인 발송
router.post('/sendtokenok', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '토큰전송완료' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));
  })    ;


 //  구매자 거래완료
 router.post('/dealcomplete', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '코인수령' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));
  })    ;


  //  거래 완료
router.post('/dealcomplete2', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "update buysell_tradetbl set trade_state = '거래완료' where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.UPDATE})
    .then(data => res.json(
      {
        'data' : data,
        'idx' : myIdx
      }
    ));
  })    ;

  
  

  

  // 상태 업데이트
  router.get('/tempadmin', function(req, res){

    var qStr = "";
    qStr += "select * from buysell_tradetbl";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  })    ;

 

  //  판매 건 수
router.post('/countsell', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "select count(*) as data from buysell_mastertbl where user_idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  })    ;


    // 구매 건 수
router.post('/countbuy', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "select count(*) as data from buysell_tradetbl where user_idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  })    ;

     // 구매 요청 건 수
router.post('/countreq', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "select count(*) as data from buysell_mastertbl bm inner join buysell_tradetbl bt on bm.idx = bt.buysell_idx ";
    qStr += "where bm.user_idx = "+myIdx+" and bt.trade_state = '구매신청'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  })    ;

     // 구매 완료 건 수
    router.post('/countcomp', function(req, res){
    const myIdx = req.body.idx || '';

    var qStr = "";
    qStr += "select count(*) as data from buysell_mastertbl bm inner join buysell_tradetbl bt on bm.idx = bt.buysell_idx ";
    qStr += "where bm.user_idx = "+myIdx+" and bt.trade_state = '거래완료'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
        {
        'data' : data
        }
    ));
    })    ;




  // 계좌 확인
router.post('/checkaccount', function(req, res){
    const myIdx = parseInt(req.body.idx || 0) || '';

    var qStr = "";
    // qStr += "select IF(account_no!='', 'TRUE', 'FALSE') as result from users_mastertbls where idx = " + myIdx;
    qStr += "select bank_code, account_no from users_mastertbls where idx = " + myIdx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });


    // 지갑 확인
router.post('/checkwallet', function(req, res){
    const user_id = req.body.user_id || '';
    const symbol = req.body.symbol || '';

    var qStr = "";
    qStr += "select IF(count(idx)>0, 'TRUE', 'FALSE') as result from mywallet_infotbl where user_idx = " + user_id + " and token_idx = '"+symbol+"'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });


//   비밀번호 변경 시 이메일 조회
router.post('/searchEmail', function(req, res){
    const phone = req.body.phone || '';

    var qStr = "";
    qStr += "select email from users_mastertbls where contact = '"+phone+"'";
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });


  //   닉네임 업데이트
router.post('/updatenick', function(req, res){
    const user_idx = req.body.user_idx || '';
    const nickname = req.body.nickname || '';

    var qStr = "";
    qStr += "UPDATE users_mastertbls SET nickname = '"+nickname+"' WHERE idx = " + user_idx;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });


// 거래정보 조회
router.post('/getDealInfo', function(req, res){
    const dealIndex = req.body.dealIndex || '';

    var qStr = "";
    qStr += "select bm.token_price as dealPrice, ti.token_krname as dealCoin, bt.token_qty as dealCount, ";
    qStr += "seller.nickname as sellerName, seller.contact as sellerPhone, seller.bank_code as sellerBank, seller.account_no as sellerAccount, ";
    qStr += "buyer.nickname as buyerName, buyer.contact as buyerPhone, buyer.bank_code as buyerBank, buyer.account_no as buyerAccount, bt.token_wallet_address as buyerWaddress ";
    qStr += "from buysell_tradetbl bt inner join buysell_mastertbl bm on bm.idx = bt.buysell_idx  inner join token_infotbl ti on bm.token_idx = ti.idx ";
    qStr += "inner join users_mastertbls seller on bm.user_idx = seller.idx inner join users_mastertbls buyer on bt.user_idx = buyer.idx where bt.idx =" + dealIndex;
    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.SELECT})
    .then(data => res.json(
      {
        'data' : data
      }
    ));
  });




  function sendAlertSms(phone, message){
    var Coolsms = require('coolsms-rest-sdk');
    var client = new Coolsms({
        key: 'NCS5UQXRAEZ4BJ48',
        secret: 'AYR8FLPJ9LABONTLVY2QWKWL4IKWKDGR'
    });
    
    client.sms.send({
        to: phone,    // recipient
        from: '15881847',  // sender
        type: 'SMS',          // SMS, LMS, MMS
        text: '[COIN25] ' + message,
        });
}

// 세자리마자 점찍기
function addComma(num) {
    var regexp = /\B(?=(\d{3})+(?!\d))/g;
    return num.toString().replace(regexp, ',');
}