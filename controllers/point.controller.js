const models = require('../models/models');
const seq = require('sequelize');
const Op = seq.Op;

// 전체 찾기
exports.index = (req, res) => {
    models.Point.findAll({
    })
    .then(data => res.json(
      {
        'data' : data
      }
    ));
};

// 1명 찾기 찾기
exports.show = (req, res) => {
  const user_idx =req.body.user_idx;
  if (!user_idx) {
    return res.status(400).json({error: 'Incorrect user_idx'});
  }

var str = ""
// str += "select a.sId as sId, b.sState as sState, b.nUsedPoint as nUsedPoint, b.nRemainPoint as nRemainPoint, b.sDetail as sDetail, DATE_FORMAT(b.createdAt, '%Y.%c.%d %H:%i') as createdAt, substring(b.followerName,1,2) as followerName "  
// str += "from users_mastertbls a left outer join point_mastertbls b on a.sId = b.sId where contact like '"+ phone +"' order by createdAt desc"
str += "select a.email as email, b.state_div as state_div, b.used_point as used_point, b.point_detail as point_detail, DATE_FORMAT(b.reg_date, '%Y.%c.%d %H:%i') as createdAt, substring(b.recommend_name,1,2) as recommend_name ";
str += "from users_mastertbls a left outer join point_mastertbls b on a.idx = b.user_idx where a.idx = "+user_idx+" order by createdAt desc";

//console.log("!!!!!"+str);
models.sequelize.query(str, { type: models.sequelize.QueryTypes.SELECT})
  .then(data => {
    if (!data) {
      return res.status(404).json({error: 'No data'});
    }

    return res.json(data);
  });
};

// 삭제  
exports.destroy = (req, res) => {
    const id = parseInt(req.params.id, 10);
    if (!id) {
      return res.status(400).json({error: 'Incorrect id'});
    }
  
    models.Point.update({
      bDelete: '1'
      }, {
      where: {
        id: id
      }
    }).then(() => res.status(204).send());
};
  
// 생성
exports.create = (req, res) => {
    const user_idx = req.body.user_idx || '';
    const email = req.body.email || '';
    const state_div = req.body.state_div || '';
    const usedPoint = parseInt(req.body.used_point) || 0;
    const point_detail= req.body.point_detail || '';
    const recommend_name = req.body.recommend_name || '';
    const recommend_email = req.body.recommend_email || '';
    const partner_code = req.body.partner_code || '';  

 
    // models.Point.create({
    //   user_id: user_idx,
    //   email: email,
    //   state_div: state_div,
    //   used_point: used_point,
    //   point_detail: point_detail,
    //   recommend_name: recommend_name,
    //   recommend_email: recommend_email,
    //   partner_code: partner_code,

    var qStr = "";
    qStr += "insert into point_mastertbls (user_idx, email, state_div, used_point, point_detail, recommend_name, recommend_email, partner_code, reg_date) ";
    qStr += "values("+user_idx +", '"+email+"', '"+state_div+"', "+usedPoint+", '"+point_detail+"', '"+recommend_name+"', '"+recommend_email+"', '"+partner_code+"', now())";

    models.sequelize.query(qStr, { type: models.sequelize.QueryTypes.INSERT})
    .then((data) => res.status(201).json(data))

};

// 업데이트
exports.update = (req, res) => {
  // console.log(req.params);
  const id = parseInt(req.params.id, 10);
  const body = req.body;

  
  const sIp = body.sIp;
  const name = body.name;
  const phone = body.phone;
  const password = body.password;
  const admin = body.admin;
  const address = body.address;
  const inday = body.inday;
  const outday = body.outday;
  const email = body.email;
  const team = body.team;
  const worktime = parseInt(body.worktime, 10);
  const unuselevel = parseInt(body.unuselevel, 10);

  models.Point.findOne({
    where: {
      sIp: sIp,
      bDelete: {
        [Op.ne]: 1
      }
    }
  }).then((Point) => {
    models.Point.update(
      {
        phone: phone? phone : Point.phone,
        password: password? password : Point.password,
        admin: admin? admin : Point.admin,
        worktime: worktime? worktime : Point.worktime,
        inday: inday? inday : Point.inday,
        outday: outday? outday : Point.outday,
        worktime: worktime? worktime : Point.worktime,
        unuselevel: unuselevel? unuselevel : Point.unuselevel,
        email: email? email : Point.email,
        team: team? team : Point.team
      }, 
      {
        where: 
        {
          id:id
        }
      }).then((Point) => res.status(200).json(Point))
  })    
  }

  