// default
const express = require('express');
const expressSession = require('express-session');
const fs = require('fs');
const ejs = require('ejs');
const passport = require('passport');
const passportLocal = require('passport-local');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');
const router = require('./controllers/index');
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const models = require('./models/models');
const sequelize = require('sequelize');
const serveIndex = require('serve-index');
const serveStatic = require('serve-static')

var https = require('https');
var http = require('http');

// import module
const bodyParser = require('body-parser');
const app = express();


// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// 위치 초기화
app.use(express.static(__dirname + '/public'));
app.use('/mimg', express.static(__dirname + '/public/manual/img'));
app.use('/ufile', express.static(__dirname + '/public/uploads'));
app.use('/.well-known/pki-validation', express.static('public/ssl'), serveIndex(__dirname + '/public/ssl', {'icons': true}));
// app.use('/.well-known/pki-validation', serveStatic(__dirname + '/public/ssl'));

// View
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);



// passport 설정
app.use(cookieParser());
app.use(expressSession({
  key: 'sid',
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 //1시간
  }
}));

// passport 초기화
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// app.use(cookieSession({
//   keys: ['cookie'],
//   cookie: {
//       maxAge: 1000 * 60 * 60 // 1시간
//   }
// }));

// module.exports = function(passport){
//   passport.serializeUser(function(user, done){
//     done(null, user.id);
//   });
//   passport.deserializeUser(function(id, done){
//     User.findById(id, function(err, user){
//       done(err, user);
//     });
//   });
// };

// router
app.use(router);

// SSL 설정
// var options = {
//   ca: [fs.readFileSync("./ssl/keeplog_kr/COMODO_RSA_Certification_Authority.crt"), fs.readFileSync("./ssl/keeplog_kr/AddTrust_External_CA_Root.crt")],
//   cert: fs.readFileSync("./ssl/keeplog_kr.crt"),
//   key: fs.readFileSync("./ssl/private-key/keeplog-kr-private-key")
// };

var options = {

  // key: fs.readFileSync("./ssl/private.key"),
  // cert: fs.readFileSync("./ssl/134590615repl_1.crt"),
  // ca:fs.readFileSync('./ssl/134590615repl_1.ca-bundle')

  // cert: fs.readFileSync("./ssl/134590615repl_1.crt"),
  // key: fs.readFileSync("./ssl/private.key")
  // ca: 
  // [
  //   fs.readFileSync("./ssl/AddTrust_External_CA_Root.crt"),
  //   fs.readFileSync("./ssl/COMODO_RSA_Certification_Authority.crt")
  // ],
  pfx: fs.readFileSync('./ssl/coin25_co.pfx'),
  passphrase: 'P@ssw0rd',
  agent: false
};

// server
var server = https.createServer(options, app);
// var server2 = express.createServer();
// server2.get('*', function(req, res) {  
//   res.redirect('https://' + req.headers.host + req.url);
// })
http.createServer(function (req, res) {
  res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
  res.end();
}).listen(80);


// app.listen(8081, () => {
// server2.listen(80);
server.listen(443, () => {
    // console.log('Example app listening!');
  
    models.sequelize.sync({force: false})
});
