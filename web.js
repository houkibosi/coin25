// default
const express = require('express');
const expressSession = require('express-session');
const fs = require('fs');
const ejs = require('ejs');
const passport = require('passport');
const passportLocal = require('passport-local');
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');
const router = require('./controllers/index');
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const models = require('./models/models');
const sequelize = require('sequelize');
const mysql = require('mysql');

// import module
const bodyParser = require('body-parser');
const app = express();


// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// 위치 초기화
app.use(express.static(__dirname + '/public'));
app.use('/mimg', express.static(__dirname + '/public/manual/img'));
app.use('/ufile', express.static(__dirname + '/public/uploads'));

// View
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);



// passport 설정
app.use(cookieParser());
app.use(expressSession({
  key: 'sid',
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 //1시간
  }
}));

// passport 초기화
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// app.use(cookieSession({
//   keys: ['cookie'],
//   cookie: {
//       maxAge: 1000 * 60 * 60 // 1시간
//   }
// }));

// module.exports = function(passport){
//   passport.serializeUser(function(user, done){
//     done(null, user.id);
//   });
//   passport.deserializeUser(function(id, done){
//     User.findById(id, function(err, user){
//       done(err, user);
//     });
//   });
// };

// router
app.use(router);

app.use(function(err, req, res, next) {
  console.log(err);

  app.use(function(error, req, res, next) {
    // 호출되지 않을 것입니다. 
    // 에러 본문의 error.toString()를 반환하는  
    // 익스프레스 기본 오류 처리기를 사용할 것입니다. 
    console.log('will not print');
    res.json({ message: error.message });
  });
  
  app.get('*', function(req, res, next) {
    setImmediate(() => { next(new Error('woops')); });
  });
});

// server
app.listen(80, () => {
    console.log('Example app listening!');
  
    // models.sequelize.sync({force: false})
});
